\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Introduction}{9}{chapter.1}
\contentsline {section}{\numberline {1.1}Thesis outline}{11}{section.1.1}
\contentsline {part}{Part~I:~Review of geometry}{13}{part.1}
\contentsline {chapter}{\numberline {2}Symplectic and toric geometry}{15}{chapter.2}
\contentsline {chapter}{\numberline {3}Contact geometry}{19}{chapter.3}
\contentsline {section}{\numberline {3.1}Toric Sasaki-Einstein manifolds}{20}{section.3.1}
\contentsline {chapter}{\numberline {4}Equivariant cohomology}{23}{chapter.4}
\contentsline {part}{Part~II:~Localization technique}{25}{part.2}
\contentsline {chapter}{\numberline {5}Finite dimensional integrals}{27}{chapter.5}
\contentsline {chapter}{\numberline {6}Infinite dimensional integrals}{30}{chapter.6}
\contentsline {part}{Part~III:~$N=1$ super Yang Mills in 7d on Sasaki-Einstein manifolds}{33}{part.3}
\contentsline {chapter}{\numberline {7}Instantons}{35}{chapter.7}
\contentsline {section}{\numberline {7.1}4d case}{35}{section.7.1}
\contentsline {section}{\numberline {7.2}7d case}{36}{section.7.2}
\contentsline {chapter}{\numberline {8}Killing spinors}{38}{chapter.8}
\contentsline {chapter}{\numberline {9}7 Dimensions and localization}{40}{chapter.9}
\contentsline {section}{\numberline {9.1}Localization technique}{43}{section.9.1}
\contentsline {section}{\numberline {9.2}$S^7$ and factorization}{46}{section.9.2}
\contentsline {part}{Part~IV:~{N=4} in 3d mapped to 1d TQM}{49}{part.4}
\contentsline {chapter}{\numberline {10}SCFT}{51}{chapter.10}
\contentsline {section}{\numberline {10.1}4d $\mathcal {N}=2$}{51}{section.10.1}
\contentsline {section}{\numberline {10.2}3d $\mathcal {N}=4$}{53}{section.10.2}
\contentsline {chapter}{\numberline {11}$S^2\times S^1$ Hypermultiplet}{56}{chapter.11}
\contentsline {chapter}{\numberline {12}Partial localization}{59}{chapter.12}
\contentsline {section}{\numberline {12.1}$S^2\times S^1$}{60}{section.12.1}
\contentsline {part}{Part~V:~$N=2$ in 4d and equivariant twisting}{63}{part.5}
\contentsline {chapter}{\numberline {13}Topological twisting}{65}{chapter.13}
\contentsline {section}{\numberline {13.1}Review of topological field theories}{65}{section.13.1}
\contentsline {section}{\numberline {13.2}Witten's $\mathcal {N}=2$ twisting}{66}{section.13.2}
\contentsline {chapter}{\numberline {14}Pestunization}{68}{chapter.14}
\contentsline {section}{\numberline {14.1}N=2 supersymmetric theories}{68}{section.14.1}
\contentsline {subsection}{\numberline {14.1.1}Global Spinors}{69}{subsection.14.1.1}
\contentsline {subsection}{\numberline {14.1.2}Vector Multiplet}{71}{subsection.14.1.2}
\contentsline {subsubsection}{Original multiplet}{71}{section*.6}
\contentsline {subsubsection}{Vector Twisting}{72}{section*.7}
\contentsline {subsection}{\numberline {14.1.3}Hypermultiplet}{73}{subsection.14.1.3}
\contentsline {subsubsection}{Twisting projectors}{74}{section*.8}
\contentsline {subsection}{\numberline {14.1.4}Localization of the gauged hypermultiplet}{76}{subsection.14.1.4}
\contentsline {chapter}{Svensk sammanfattning}{80}{section*.9}
\contentsline {chapter}{References}{82}{section*.10}
