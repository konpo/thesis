%
% Template for Doctoral Theses at Uppsala 
% University. The template is based on    
% the layout and typography used for      
% dissertations in the Acta Universitatis 
% Upsaliensis series                      
% Ver 5.2 - 2012-08-08                  
% Latest version available at:            
%   http://ub.uu.se/thesistemplate            
%                                         
% Support: Wolmar Nyberg Akerstrom        
% Thesis Production           
% Uppsala University Library              
% avhandling@ub.uu.se                          
%                                         
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\documentclass{UUThesisTemplate}

% Package to determine wether XeTeX is used
\usepackage{ifxetex}

\ifxetex
	% XeTeX specific packages and settings
	% Language, diacritics and hyphenation
	\usepackage[babelshorthands]{polyglossia}
	\setmainlanguage{english}
	\setotherlanguages{swedish}

	% Font settings
	\setmainfont{Times New Roman}
	\setromanfont{Times New Roman}
	\setsansfont{Arial}
	\setmonofont{Courier New}
\else
	% Plain LaTeX specific packages and settings
	% Language, diacritics and hyphenation
    % Use English and Swedish languages. 
	\usepackage[english]{babel} 

	% Font settings
	\usepackage{type1cm}
	\usepackage[utf8]{inputenc}
    \usepackage{lmodern}
	\usepackage[T1]{fontenc}
    \usepackage[babel=true]{microtype}
	%\usepackage{mathptmx}
	
	% Enable scaling of images on import
	\usepackage{graphicx}

\fi


% Tables
\usepackage{booktabs}
\usepackage{tabularx}
\usepackage{multirow}

% Document links and bookmarks
\usepackage{hyperref}

% Math
\usepackage{psfrag,slashed,cancel,lscape,caption,array}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{mathrsfs}
\usepackage{dsfont}
\usepackage{shuffle}

%\renewcommand{\appendixname}{appendix}

% Misc
\usepackage[page,title,titletoc]{appendix}
\usepackage{cite}
\usepackage{todonotes}
\newcommand{\TODO}[1]{{\tikzexternaldisable\todo{#1}\tikzexternalenable}}
\usepackage{epigraph}
\usepackage{bbding}

% Numbering of headings down to the subsection level
\numberingdepth{subsection}

% Including headings down to the subsection level in contents
\contentsdepth{section}

% Tikz
	\usepackage{tikz}
	\usepackage{tikz-cd}
\usetikzlibrary{
	decorations.markings
}
\tikzset{
	->-/.style={
		decoration={
			markings,
			mark=at position #1 with {\arrow{latex}}},
		postaction={decorate}
	},
	->-/.default=0.5
}
\tikzset{
	partial ellipse/.style args={#1:#2:#3}{
		insert path={+ (#1:#3) arc (#1:#2:#3)}
	}
}
\usepackage{tikz-3dplot}
\usepackage{tikz-cd}

% Uncomment to use a custom abstract dummy text
\abstractdummy{
  \begin{abstract}
  Quantum Field Theory has been a dominating framework in elementary particle physics during the last century. Within this framework, supersymmetric theories have attracted a lot of attention due to their mathematical structure, simplicity and insight into the problems of unification, dark matter and hierarchy. Even though the boundaries of our perturbative undestanding of supersymmetric theories have been pushed far, there is generically no systematic way to obtain exact results.
  Especially for strongly coupled theories, where perturbative techniques cannot be applied, methods for exact computations are crucial. A powerful technique to obtain exact results of partition functions and correlators of curtain protected operators is \emph{supersymmetric localization}. This thesis studies physical and geometrical properties of localization results in different supersymmetric theories.
    
  The first model considered is maximally supersymmetric Yang-Mills placed on a Sasaki-Einstein 7-dimensional manifold. After redefining the fields to differential forms, the cohomological complex is formed and a localization computation of the partition function is performed using data from the moment map cone. This procedure and the factorization properties reveal a strong structural dependence of the result on the geometry. A second part discusses N=4 matter-multiplets in 3d. We identify BPS operators supported on a 1-dimensional submanifold. Applying the localization formula, the partition function simplifies to a topological partition function of a dual topological quantum mechanics. Lastly, we perform an equivariant twisting on N=2 gauge theories with matter on 4-dimensional manifolds with a torus action. This is achieved by a global field redefinition leading to differential forms or spinors that are defined on a large class of manifolds. The resulting cohomological theory admits two kinds of fixed points which are treated differently in a localization computation. 
  \end{abstract}
}

% shorthands



\newcommand{\nn}{\nonumber}


% cpp symbol
\usepackage{graphicx,relsize} 
\makeatletter
\DeclareRobustCommand\Cpp{{\em C\raisebox{2pt}{{\relsize{-2}++}}} }
\makeatother

\begin{document}
\frontmatter
    % Creates the front matter (title page(s), abstract, list of papers)
    % for either a Comprehensive Summary or a Monograph.
    % Authors of Comprehensive Summaries use this front matter 
    \frontmatterCS 
    % Monograph authors use this front matter 
    %\frontmatterMonograph 
 
   % Optional dedication
   %\dedication{Dedicated to all hard-working \\doctoral students at Uppsala University}
   %\dedication{To everyone that ever made me laugh}
   \dedication{\ldots}
 
    % Environment used to create a list of papers
    \begin{listofpapers}
    \item K. Polydorou, A. Roc\'en and M. Zabzine, {\it 7D supersymmetric Yang-Mills on curved manifolds}, JHEP {\bf 12} (2017) 152, arXiv:1710.09653 [hep-th]. \label{lbl:paper1}
    \item R. Panerai, A. Pittelli,  and K. Polydorou,, {\it Topological Correlators and Surface Defects
    	from Equivariant Cohomology}, \label{lbl:paper2} 
    \item G. Festuccia, A. Gorantis, A. Pittelli, K. Polydorou, and L. Ruggeri, {\it CohomologicalLocalization of $N= 2$ Gauge Theories with Matter}, \label{lbl:paper3}
    \end{listofpapers}
    
    
    \begingroup
        % To adjust the indentation in your table of contents, uncomment and enter the widest numbers for each level
        %  E.g.  \settocnumwidth{widest chapter number}{widest section number}{widest subsection number}...{...}
       %  \settocnumwidth{5}{4}{5}{3}{3}{3}
        \tableofcontents
    \endgroup
    
% Optional tables
%\listoftables
%\listoffigures

\mainmatter
% This includes the "Instruction", "Problem and Solutions" and "Example" files. After reading it, remove it from Thesis.tex.
\input{parts/intro.tex}
\input{parts/Background.tex}
\input{parts/Localization.tex}
\input{parts/sevendimensions.tex}
\input{parts/TwistedOperators.tex}
\input{parts/Twisting.tex}
%\input{parts/Acknowledgements.tex}
\input{parts/SwedishSamary.tex}

    

\backmatter
    % References
    % No restriction is set to the reference styles
    % Save your references in References.bib
    % \nocite{*} % Remove this for your own citations
    \bibliographystyle{JHEP}
    \bibliography{References}

\end{document}

