\part{Localization technique}\label{part:localization}
 Localization gives a way to compute partition functions and BPS correlators but more mathematically it helps computing integrals of equivariant closed forms in terms of the fixed locus of the group action. We will first review the mathematical formula of Berline-Verne-Atiayh-Bott on equivariant localization which is performed on finite dimensional integrals. Then look at the supersymmetric localization technique in a general supersymmetric field theory. The difference here is that we use the localization technique on path integrals which are integrals on an infinite dimensional space.
 
 While the most complete review about the subject is  \cite{Pestun:2016zxk} one can find other pedagogical introductions such as \cite{Cremonesi:2014dva}.
 
\chapter{Finite dimensional integrals}\label{Sec:generalloc}

For simplicity we will restrict ourselves in the case of a $U(1)$ action however one can generalize this to more complicated actions. 

Let us assume $M$ to be a compact $n$-dimensional manifold  with a $U(1)$ action with isolated fixed points and $v$ be the vector field associated with this $U(1)$. Let also, $\alpha$ be a equivariant closed polyform \footnote{A polyform is a form of mixed degrees $\alpha = \alpha_n + \alpha_{n-1}+\ldots+\alpha_0$ as introduced in \eqref{eq:polyform}.}, i.e. $(d+ \iota_v)\alpha=0$. The Berlin-Vergne-Atiyah-Bott formula is
\begin{equation}\label{loc:atiya-bott}
\int_M \alpha = \sum_{i} (2\pi)^n \frac{\alpha_0 (x_i)}{\sqrt{det (\partial_\mu v^\nu (x_i)) }} \, ,
\end{equation}
 where $x_i$ are the fixed points of $U(1)$.
 We will review the derivation of the formula since it generalizes easily to the quantum field theory case, For a thorough review we refer the reader to \cite{Pestun:2016jze}. We consider the odd tangent bundle $\Pi T M$ with coordinates $(x^\mu, \psi^\mu)$, as it was introduced in Section \ref{sec:equivariantcohomology}. Using the supergeometric approach, $x^\mu$ are the coordinates on $M$ and $\psi^\mu$ are the Grassmann coordinates on the fibre.
 We also define the following supersymmetric like transformations \eqref{eq:easytranformation}
 \begin{align}
 d_v x ^\mu = \psi^\mu\, , \\
 d_v \psi^\mu = v^\mu \, 
 \end{align}
 Then the integral of $\alpha$ over $M$ becomes
 \begin{equation} \label{loc:math-original-action}
 Z= \int_{\Pi TM}d^n x\, d^n \psi \,\, \alpha(x,\psi)
 \end{equation}
 
 One can note that the equivariant differential acts the same way as a supersymmetric transformation that squares to $d_v ^2 = \mathcal{L}_v$ and since $\alpha$ is equivariantly closed it can be interpreted as a supersymmetric observable, for example the exponential of a supersymmetric action. 
 
 We consider the deformation of \eqref{loc:math-original-action} with respect to the real parameter, $t\in \mathbb R$,
 \begin{equation}\label{loc:math-deformed-action}
 Z(t)= \int_{\Pi TM}d^n x \,d^n \psi\,\, \alpha(x,\psi)\,\,e^{-t d_v W(x,\psi)}\, ,
 \end{equation}
 where $W(x,\psi)$ is a function such that $d_v^2 W = \mathcal{L}_v W =0$. One can show easily that $Z(t)$ is independent of $t$
 \begin{align}
 \frac{d}{dt}Z(t)=& \int_{\Pi TM}d^n x\, d^n \psi \big[ d_v W(x,\psi)\big]\, \alpha(x,\psi)\,\,e^{-t\, \,d_v W(x,\psi)}\\
 =&  \int_{\Pi TM}d^n x \,d^n \psi  \,\,d_v\big[ W(x,\psi)\,\, \alpha(x,\psi)\,\,e^{-t\,\, d_v W(x,\psi)}\big]=0\, ,
 \end{align}
 where in the second line we have integrated by parts and used that $\alpha$ and $d_v W$ are equivariantly closed. Finally since $M$ is a compact manifold we use the Stokes theorem to set the integral to zero. This is very convenient since the original integral \eqref{loc:math-original-action} can be exactly calculated by \eqref{loc:math-deformed-action} for any choice of $t$. The choice which simplifies the integral is for $t\to \infty$. Then from saddle point approximation the main contribution will arise from $d_v W =0$.
 
 Indeed we are going to investigate this in the convenient choice of $W$: 
 \begin{equation}
 W= g_{\mu\nu} \psi^\mu (d_v\psi)^\nu\, ,
 \end{equation}
 for $g$ being the metric on $M$ which is invariant under the group action i.e $\mathcal{L}_v g=0$. Here $v$ is a Killing vector.
For this choice of $W$, we have 
\begin{equation}
d_v W = |v|^2 + \partial_\lambda (g_{\mu\nu} v^\mu)\psi^\nu \psi^\lambda\, .
\end{equation}
Notice that the first term is a zero form and it is semi-positive definite. As a result the exponential in \eqref{loc:math-deformed-action} is dominated at $t\to \infty$ by the fixed points $x_i$ of the $U(1)$ action, $v(x_i)=0$.

Now let us consider the contribution of one isolated fixed point $x_{f.p.}$ and rescale the coordinates 
\begin{align}
x \to \tilde{x}= \sqrt{t}\, x, \quad 
\psi \to \tilde{\psi}= \sqrt{t} \,\psi\, ,
\end{align}
so that the measure of the integral \eqref{loc:math-deformed-action} stays invariant.

Also we have that the exponent becomes
\begin{equation}
t\, d_v W =  H_{\mu\nu}\delta \tilde x^\mu \delta \tilde x^\nu + S_{\mu\nu}\tilde{\psi^\mu}\tilde{\psi^\nu} +  \mathcal{O}(\frac{1}{t})\, ,
\end{equation}
where $\delta \tilde x = \tilde x- x_{f.p.}$ is the distance from the fixed point. The functions $H$ and $S$ are of the form
\begin{align}
H_{\mu \nu}=&g_{\lambda\rho}( \partial_\mu v^\lambda \,\,\partial_\nu v^\rho)|_{ x_{f.p.}} \, ,\\
S_{\mu\nu}=& g_{\mu\lambda}\,\,\partial_\nu v^\lambda |_{x_{f.p.}}\, .
\end{align}

As we see the exponential is quadratic and does not depend on $t$. The leading contribution then is a top form which has as a result that only the zero form from $\alpha$ will survive. In particular it is only supported on the fixed point because all the other points give contributions exponentially suppressed. [explain more!]

In this case the integral can be performed since it separates into two Gaussian integrals, one with real integration parameters and one with Grassmann
\begin{equation}
Z[0]= \lim_{t\to\infty } Z[t]= (2 \pi )^n \alpha_0 |_{x_{f.p.}}\frac{Pf(S)}{\sqrt{\det(H)}}.
\end{equation} 
By substituting $S$ and $H$ and summing over all the fixed points we get the  Berlin-Vergne-Atiyah-Bott formula \eqref{loc:atiya-bott}.

One can follow the same steps and find a similar formula for a fixed submanifold locus.
In that case we will have that if $F \subset \mathcal M$ is the submanifold fixed locus then we suspect that the integral will have only contributions of the top component of the restriction of $\alpha$ on $F$. Indeed, the formula reads, \cite{Atiyah:1984px}, \cite{Pestun:2007rz}
\begin{equation}\label{eq:localizationformulasubmanifold}
\int_{\mathcal{M}} \alpha = \int_{\mathcal F} \frac{\iota^\ast_F \alpha}{e(N)}\, ,
\end{equation}
where $\iota^\ast_F \alpha$ is the pullback of $\alpha$ onto $F$ and $\mathcal N$ is the normal bundle.

\chapter{Infinite dimensional integrals}\label{sec:localizationgaugetheories}
Let us turn to explain how the argument modifies in the case of QFTs.

Consider a supersymmetric quantum field theory with supersymmetric transformation $\delta$ (usually Grassmann odd). The goal is usually to calculate observables of the theory such as the partition function or correlators of local operators.

We will start by considering a partition function
\begin{equation}
Z= \int [D\phi] e^{-S[\phi]}\, ,
\end{equation}
where we represent all the fields of the theory with $\phi$. The difference between this and the previous case is that here we are integrating over the space of field-configurations which is an infinite dimensional space. 


The square action of supersymmetry on the fields $\phi$ consists of symmetries of the theory. In the case where there is a torus action acting on the manifold then the usual operator takes for example the form \eqref{eq:delta2equiv}. This supersymmetric operator will play the role of the equivariant differential $d_v$.

Let us follow the same steps as before and deform the action using an $\delta$-exact term
\begin{equation}
Z_t = \int [D\phi] e^{-S[\phi] -t \delta V}\, .
\end{equation}
As we explained in the previous section $Z_t$ is independent of the parameter $t$. 

The canonical choice of the deformation term $V$ is 
\begin{equation}\label{eq:deftermgeneral}
V=\sum_{ \Psi} (\delta \Psi)^\dagger \Psi \, ,
\end{equation}
where $\Phi$ refers to fermionic terms. The resulting  deformation action has as bosonic part 
\begin{equation}
(\delta V)_{\text{bos}}= \sum_{ \Psi}  |\delta \Psi|^2 \, .
\end{equation}
This is positive semi-definite. In this case the main contribution to the partition function will be given from $(\delta V)_{\text{bos}}=0$ which results in a localization locus
\begin{equation}
\delta\Psi =0 \, .
\end{equation}
Note that in the case where we sum over fermionic terms less than the fermionic degrees of freedom then our localization locus is not as restricted. 

As before then one can expand the fields around their localization locus value $\phi_0$ as $\phi\to \phi_0 + t ^{-1/2} \phi'$, where $\phi'$ is the fluctuation of the field. 

At the limit where $t\to +\infty$ the only terms surviving will be the classical part that contains only $\phi_0$ terms and a one-loop contribution
\begin{equation}\label{eq:generallocgauge}
Z= \int [D\phi_0] e^{-S[\phi_0]} \frac{1}{\text{sdet}(\delta^2)}\, ,
\end{equation}
where the superdeterminant means that the contribution of the bosonic fields will be in the denominator of \eqref{eq:generallocgauge} and the contribution of the fermions in the numerator.


Except from the partition function one can consider correlation functions of gauge invariant operators $\mathcal O$. However in order for the localization argument to go thought in the presence of such operators, they need to be $\delta$-closed. Operators with such a condition, i.e $\delta \mathcal O=0$, are called BPS-operators or protected operators and therefore are very interesting since one can perform exact calculations of their correlators. 

A challenging part of \eqref{eq:generallocgauge} is to calculate the 1-loop contribution. There are many ways of performing the superdeterminant as we will hint in the following chapters. One way is by explicitly finding the spectrum of the fluctuations under the $\delta^2$ action. One can follow \cite{Kapustin:2009kz}, \cite{Kim:2012ava} and \cite{Gorantis:2017vzz} for examples of this approach in three, five and various dimensions respectively. 

Another way is by an index theorem \cite{Atiyah:1984px}, where an finite dimensional example we saw in the previous Section. In odd dimensions one can prove that the index is associated to counting points on the moment map cone of your manifold as we discuss in Section \ref{sec:7dlocalizationpart} in the context of 7-dimensional Sasaki-Einstein manifolds. This is an additional simplification occurring in odd dimensions. Related work in the 5-dimensional case can be found in \cite{Qiu:2014oqa}. Another way is to explicitly calculate the index under the torus action as we explain in Section \ref{sec:twistinglocalization}. This approach is mostly used in even dimensions. [rephrase the full paragraph]

Finally there are some points that need to be stressed. Firstly in the case of gauge theories the action needs to be gauge fixed. Then one can consider as the appropriate operator the supersymmetric variation together with the BRST-transformation. [maybe explain why I want both?] Another point is that as it has become apparent there is freedom in the choice of supercharge. There is also freedom in the deformation term that is used. Different deformation terms result to different localization schemes. The result of different schemes is the same after algebraic manipulations of the result [cite??]. Finally the answer needs to be regularized and again there are many different regularization schemes as we also glims in Section \ref{sec:7dlocalizationpart}. For these reasons, often different studies result to different partition functions of the same theory. 
