\part{{N=4} in 3d mapped to 1d TQM}\label{part:twistedoperators}

As we have already pointed out, localization is a very powerful tool that helps us calculate exactly partition functions and correlators of certain local operators of supersymmetric theories. Theses operators have to be $\delta$-closed or in other words in a protected sector with respect to the localizing supercharge as mentioned in Section \ref{sec:localizationgaugetheories}. This is one of the reasons why \cite{Beem:2013sza} gained some interest. They studied $\mathcal{N}=2$ SCFT in 4d, from the Conformal Bootstrap perspective, and defined new operators that are supported only on a 2-dimensional plane and lie in the cohomology of a specific supercharge. Most importantly the correlation functions of these operators define an associated chiral algebra.
This means that these unconventional operators create a map between $\mathcal{N}=2$ SCFT in 4d and 2d chiral algebras.

In this Part we are going to briefly review the work of \cite{Beem:2013sza}, focusing on the algebra. Then we will turn out attention to three dimensions where we are going to apply the same trick firstly in flat space, \cite{Chester:2014mea}, and then in the case of $S^2\times S^1$. In light of the results we will explain a different approach on the matter that was initially introduced by \cite{Dedushenko:2016jxl} and then generalized in Paper \ref{lbl:paper2}. Finally we will make some notes of the importance of the results of Paper \ref{lbl:paper2}.

\chapter{SCFT}\label{sec:schurscft}
\section{4d $\mathcal{N}=2$}\label{sec:rastelli4d}
The superconformal algebra of $\mathcal{N}=2$ is 4d on flat space is $sl(4|2)$. The authors of \cite{Beem:2013sza} identified a subalgebra $sl(2)\oplus \widehat{sl(2)}\subset sl(4|2)$, where a specific nilpotent supercharge $\mathcal{Q}$, $\mathcal{Q}^2 =0$, acts with a specific way on the bosonic algebra.

Before proceeding in identifying this algebra we would like to explain the general strategy that the authors in \cite{Beem:2013sza} approached in order to use it throughout the next Sections. 
{\color{red} Change it. STart with a plane and explain without the sl(2) in the start.}

\begin{itemize}
	\item First a nilpotent supercharge $\mathcal Q$ is identified.
	\item An $sl(2)$ subalgebra of the bosonic algebra is identified. This $sl(2)$ algebra corresponds to a subalgebra of the symmetry acting on a specific hyperplane (or submanifold $F$ more generally).
	\item The $R$ symmetry generators may be used in order to twist the $sl(2)\to \widehat{sl(2)}$ to become $\mathcal Q$-exact. 
	\item We identify a operator in the $\mathcal Q$ cohomology on a point of the manifold (usually the origin) using the $\widehat{sl(2)}$ and maybe additional constraints. 
	\item The operator is translated along $F$ using the twisted algebra and potentially other parts of the subalgebra on $F$ that preserve the cohomology. These are twisted-translated operators.
\end{itemize}


Let us now turn the the model at hand and identify the algebra. The $\mathcal{N}=2$ superconformal algebra in 4d has as a maximal bosonic subalgebra the $S0(6)\oplus sl(2)_R \oplus U(1)$.\footnote{In this chapter we are going to assume all algebras are over the field of complex numbers.} The $SO(6)$ is the conformal algebra in four dimensions with generators: the translations $P$, Lorentz transformation $M$, dilatation $D$ and special conformal transformations $K$. The R-symmetry generators are an Abelian factor $r$ and the generators $R^\pm, \, R$ in the Chevalley basis, \cite{Dolan:2002zh}. Finally there are 16 fermionic generators, 8 Poincaré $Q^I{}_\alpha, \tilde{Q}_{I\dot{\alpha}}$ and 8 conformal supercharges $S_I{}^\alpha, \tilde{S}^{I\dot{\alpha}}$. The Greek letters $\alpha, \, \dot\alpha$ are left and right handed spinor indices respectively and $I, \,J$ the $R$-symmetry indices. We are not going to write the algebra but the interested reader can be referred to Appendix A1 of \cite{Beem:2013sza}. 

Let us concentrate our attention to a specific plane $\mathbb{R}^2 \subset \mathbb{R}^4$. There are two relevant rotation generators, $M^\parallel$ that acts within the plane and $M^\perp$ that rotates a vector orthogonal to the plane. One can define complex coordinates on $\mathbb{R}^2$, ($z,\bar z$), and hence the holomorphic generators of $sl(2)$, which are the holomorphic part of the algebra action on the plane, can be expressed
\begin{equation}
L_{-}= - \partial_z, \quad L_+= -z^2 \partial_z,\quad L_0 = - z \partial_z\,.
\end{equation}  
These generators can be also identified with part of the conformal algebra in 4d, $L_-= P_{+\dot +}$, $L_+= K^{\dot +  +}$ and $2L_0 = M^\parallel+ D$. Here the translation and special conformal transformation are appropriately contracted with the sigma matrices.

The anti-holomorphic generators on the plane together with a subset of the supercharges form $sl(2|2)$. The  anti-holomorphic part of the conformal algebra on the plane is
\begin{equation}
\bar{L}_- =P_{-\dot -}= - \partial_{\bar z}, \quad \bar L_+= K^{\dot -  -}= -\bar z ^2 \partial_{\bar z},\quad\bar L_0 ={1\over 2}( D-M^\parallel)= - \bar z \partial_{\bar z}\,.
\end{equation}
and the fermionic generators are
\begin{equation}
Q^I = Q^I {}_-,\quad S_I = S_I{}^-  \quad  \tilde Q _I = \tilde{Q}_{I\dot -}, \quad \tilde S = \tilde S ^{I \dot{-}}\, .
\end{equation}
Finally the algebra has a central element $Z = M^\perp +r$.

One can find the following nilpotent supercharges 
\begin{equation}
\mathcal{Q}_1= Q^1 + \tilde S ^2 \quad\text{and}\quad \mathcal{Q}_2 = S_1 - \tilde Q _2,
\end{equation}
 which commute with the holomorphic $sl(2)$.
 
 Now that the supercharges are identified one can find the R-twisted $\widehat{sl(2)}$ that is $\mathcal{Q}_{1,2}$-exact. Its generators are 
 \begin{align}
 \hat L_+ &= \bar L_+ - R^+ = \{ \mathcal{Q}_1, \tilde{Q}_1\}=-\{ \mathcal{Q}_2, Q^2\} \\
 \hat L_- &= \bar L_- + R^- = \{ \mathcal{Q}_1, S_2\}=\{ \mathcal{Q}_2, \tilde S^1\} \\
 \hat L_0 &= \bar L_0 - R= \{ \mathcal{Q}_1, \mathcal{Q}_1^\dagger\}=\{ \mathcal{Q}_2, \mathcal{Q}_2^\dagger\}\, .
 \end{align}
In fact the central extension is also $\mathcal{Q}_{1,2}$-exact
 \begin{equation}\label{centralcharge4d}
 \mathcal{Z}= - \{\mathcal{Q}_1, \mathcal{Q}_2 \}\, .
 \end{equation}
 This twisted algebra still acts antiholomorphicaly on the plane. 
 
 Since we have identified the algebra now we can look into the local operators. Let us assume that the desired operator is inserted at the center of $\mathbb{R}^4$ and we would like it to be in an non-trivial cohomology of both supercharges $\mathcal{Q}_{1,2}$. Because of this for simplicity we will define a new family of supercharges $\mathcal{Q}= \mathcal{Q}_1 + \xi \mathcal{Q}_2$ for arbitrary parameter $\xi$. Then the operator that is inserted at zero, $\mathcal{O}(0)$, is a representative of the non-trivial $\mathcal{Q}$-cohomology if 
 \begin{equation}\label{cohomology4d}
 \{ \mathcal{Q}, \mathcal{O}(0) ] =0 \quad \text{and}\quad \mathcal{O}(0)\neq \{Q,\mathcal{O}'(0)]
 \end{equation}
 where here the appropriate commutator/anti-commutator is used depending on the nature of the operator.
 
 One can identify these operators by their quantum numbers. Since $\hat{L}_0$ and $Z$ are both $\mathcal{Q}$-exact and they commute with $\mathcal{Q}$, the operator lives in the zero-eigenspace of both. This translates to the following conditions for these operators.
 \begin{align}
\frac{1}{2}(E - j_1 -j_2) -R =0 \quad \text{and} \quad r+ j_1 - j_2=0
 \end{align}
 where $E$  is the eigenvalue of $H$, (the conformal dimension), $j_{1,2}$ are Lorenz quantum numbers and finally $R$ is the R-charge. These operators were called Schur operators since they coincide with those that survive in the Schur limit of the superconformal index, \cite{Kinney:2005ej, Gadde:2011ik, Gadde:2011uv}.
 
 Until now we identified the operators at the origin of $\mathbb{R}^2$. Then we can twist-translate the operator on the plain using the above algebras
 \begin{equation}
 \mathcal{O}(z, \bar{z}) = e^{z L_{-} + \bar{z}\hat L_{-}}\mathcal{O}(0) e ^{-z L_{-} - \bar{z}\hat L_{-}}
 \end{equation}
 where also $\{ \mathcal{Q}, \mathcal{O}(z,\bar{z}) ]=0$, since $L_-$ is $\mathcal{Q}$-closed and $\hat L_-$ is $\mathcal{Q}$-exact with $\mathcal{Q}$ nilpotent. Also  because of that, if $\mathcal{O}(z,\bar z) = \{ \mathcal{Q}, \mathcal{O}' (z,\bar{z})]$ this means that $ \mathcal{O}(0)= \{Q,\mathcal{O}'(0)]$ which is not the case. This proves that $\mathcal{O} (z,\bar{z})$ defines a $\mathcal{Q}$ cohomology class on the plane. 
 
 This operators can be defined also using the spin representation $k$ of $sl(2)_R$, $\mathcal{O}^{I_1,\ldots,I_{2k}}$, with $I_i=1,2$, as
 \begin{equation}\label{4dspinrep}
 \mathcal{O}(z,\bar{z})= u_{I_1}\ldots u_{I_{2k}}\mathcal{O}^{I_1,\ldots,I_{2k}}
 \end{equation}
where the dressing factor $u_I = (1,\bar{z})$. {\color{red}Explain it differently. these ops can be defined as follows. LEt the O be an op transforming... Also show why these ops have antiholomorphic correlators. }

In \cite{Beem:2013sza} it was shown that these operators have meromorphic correlators up to a $\mathcal Q$-exact term. The operators form an infinite-dimensional chiral algebra which establishes a map between 4d CFTs and chiral algebras in two dimensions. 

{\color{red}Add example of scalars}

\section{3d $\mathcal{N}=4$}

Our attention is turned to three dimensions and we assume that we are in $\mathbb{R}^3$ with coordinates $x=(x_1,x_2,x_3)$. This part will review the work of \cite{Beem:2016cbd}. The total superconformal algebra is $osp(4|4)$. It contains as a subalgebra the $su(2|2)$ which is the superconformal algebra of a 1-d SCFT with 8 real supercharges. This algebra is the one we are going to twist in a similar fashion with the 4d case.

Let us briefly analyze the superconformal algebra in three dimensions. Its maximal bosonic subalgebra is
\begin{equation}
osp(4|4)\supset so(3,2)\oplus su(2)_C \oplus su(2)_H\, .
\end{equation}

The 3d conformal algebra consists of $P_\mu, M_{\mu \nu}, D,K_\mu$ which are the generators of translations, Lorentz transformations, dilatation and superconformal transformations respectively ($\mu, \nu= 1,2,3$).  The generators of the left and right R-symmetries, $su(2)_C$ and $su(2)_H$, are respectively \footnote{The right and left R-symmetries are also called Coulomb and Higgs respectively} $R_a{}^b$ and $\bar R_{\dot{a}}{}^{\dot{b}}$, where the undotted indices are $su(2)_C$ (Coulomb) indices and the doted ones are $su(2)_H$ (Higgs) indices. Finally the super-generators of $osp(4|4)$ are eight Poincar\'e supercharges $Q_{\alpha a \dot a}$ and eight conformal supercharges $S^\alpha{}_{a\dot a}$, where $\alpha = 1,2$ is the spinor index. 

Now one can consider the subalgebra $su(2|2)$ that we are interested in as follows. We can separate $\mathbb{R}^3$ in $\mathbb{R}\times \mathbb{R}^2$ where we consider $(x_2, x_3)\in \mathbb{R}^2$. Then the $su(2|2)$ is created by 1d conformal transformations $sl(2)$ which will be the generators that act on the line $P\equiv P_1$, $K\equiv K_1$, $D$ and an $su(2)_R$ R-symmetry, which we can choose to identify with the $su(2)_C$. By looking at the $osp(4|4)$ superalgebra \footnote{Here we are not rewriting the algebra but for the interested reader one can consult \cite{Chester:2014mea} equations (B.31)-(B.37) and check for which supercharges the algebra with only $P,\, K,\, D$ and $R$ closes to eachself. This can be done up to an $su(2)_R$ rotation.} one can consider as fermionic generators of the $su(2|2)$ the  $Q_{1a\dot 2}$, $Q_{2a\dot{1}}$, $S^1{}_{a \dot{1}}$ and $S^2{}_{a\dot{2}}$. Finally the same way as in the 4d case there is a central extension $Z\equiv i M^{\perp}- R^{\dot 1}{}_{\dot 1}$, where $M^\perp$ is the Lorentz transformation that has the line as a fixed locus.

The strategy is similar as in 4d:
\begin{itemize}
	\item Firstly we will find supercharges $\mathcal{Q}$ which are nilpotent i.e. $\mathcal{Q}^2 =0$. There are several choices and one of them is 
	\begin{align}
	\mathcal{Q}_1 = Q_{11\dot{2}}+ S^2{}_{2\dot{2}}
	\end{align}
	which is the choice of \cite{Beem:2016cbd}. It should be noted that again the commutator of the two supercharges gives the central charge
	\begin{equation}\label{commutatorqmath}
	\{ \mathcal{Q}_1, \mathcal{Q}_2 \} = -8 i \mathcal{Z}
	\end{equation}
	as in \eqref{centralcharge4d}.
	\item We are going to twist the bosonic $sl(2)$ in such a way that the twisted operators are going to be $\mathcal{Q}_{1,2}$-exact.
	The generators are
	\begin{align}
	\hat{L}_- = P+ i R_2{}^1 = - \frac{1}{4} \{\mathcal{Q}_1, Q_{22 \dot 1}\}= \frac{1}{4} \{\mathcal{Q}_2, Q_{12 \dot 2}\}\, ,\\
	\hat{L}_+ =  K+ i R_1{}^2 = - \frac{1}{4} \{\mathcal{Q}_1, S^1{}_{1 \dot 1}\}=\frac{1}{4} \{\mathcal{Q}_2, S^2{}_{1 \dot 2}\}\, ,\\
	\hat{L}_0 = -D + R_1{}^1= -\frac{1}{8} \{\mathcal{Q}_1, \mathcal{Q}^\dagger_1\} =-\frac{1}{8} \{\mathcal{Q}_2, \mathcal{Q}^\dagger_2\} \, ,
	\end{align}
	with them being $sl(2)$ generators, $[\hat{L}_0, \hat{L}_\pm]= \pm \hat{L}_\pm$ and $[\hat{L}_+, \hat{L}_- ]= -2\hat{L}_0$. This algebra is $su(2)_R$ twisted with respect to the original one so we will refer to it as $\widehat{sl(2)}$. 

From now on we are going to consider the one parameter family $\mathcal{Q}= \mathcal{Q}_1 + \xi \mathcal{Q}_2$, where $\xi$ is a parameter. 
\item The operators at the origin of the plane $\mathcal{O}(0)$, which satisfy some non-trivial $\mathcal{Q}$-cohomology same as in \eqref{cohomology4d}.
\item One can use the $\hat L_-$ operators to translate them along the line
\begin{align}
\mathcal{O}(x_1) &= e^{-i x_1 \hat L _-} \mathcal{O}(0) e^{i x_1 \hat L _-}\\
&=u^{I_1} (x_1)\ldots u^{I_1} (x_k) \mathcal{O}_{I_1 \ldots I_k}
\end{align}
with $u^I \equiv (1,x_1)$ similar to the 4d spin representation \eqref{4dspinrep}. These operators are the desired operators on the line. Note that since the submanifold in one dimensional, the only needed generator for translating the operators is the one contained in the twisted $\widehat{sl(2)}$. 

{\color{red}the correlators are topological, explain what does that means and consider hyper with scalars.}

\end{itemize}


\chapter{$S^2\times S^1$ Hypermultiplet}\label{section:s2s1}

The goal is to apply this procedure in theories that do not have super-conformal symmetries. The first instance was in $S^3$ by \cite{Dedushenko:2016jxl} where they considered $\mathcal{N}=4$ hypermultiplets.


In this section we are considering supersymmetry on $S^2\times S^1$. In particular we consider the following choice of coordinates 
\begin{equation}\label{eq:metrics2s1}
ds^2 = r^2 \beta^2 dt^2 + r^2(d\theta^2 + \sin^2\theta d\phi^2).
\end{equation}
where $t$ is the coordinate on $S^1$ and $\beta$ is its radius. The other two coordinates are parametrizing the $S^2$ as one can see from \ref{FIG:s2s1_coordinates}. Notice that in these coordinates the $S^2$ is written as a fibre of $S^1$ over an interval.
\begin{figure}[h!]
	\centering
	\begin{tikzpicture}[thick,scale=1.3,>=latex]
	\begin{scope}[xshift=-2.1cm]
	\path[use as bounding box] (-1.6,-1.6) rectangle (1.6,1.6);
	\colorlet{wilsonline}{cyan}
	\colorlet{shadow}{black!10}
	\begin{scope}
	\clip (-1.5,-1.5) rectangle (1.5,1.5);
	\fill [shadow] (0,0) circle[radius=1.5];
	\fill [white] (-0.1,0.5) circle[radius=1.7];
	\end{scope}
	\draw[->-=0.53, black!70] (0,0) [partial ellipse=-180:0:1.5 and 0.68];
	\draw[dashed, black!70] (0,0) [partial ellipse=0:+180:1.5 and 0.68];
	\draw[->-=0.25, black!70, rotate=115] (0,0) [partial ellipse=-35:-180:1.5 and 1.0];
	\draw[dashed, black!70, rotate=115] (0,0) [partial ellipse=145:+180:1.5 and 1.0];
	\draw[ultra thick, black] (0,0) circle (1.5);
	\fill [black](0,1.35) ellipse (1.5pt and 1pt);
	\fill [black!50](0,-1.35) ellipse (1.5pt and 1pt);
	\node at (0.8,1) {$\theta$};
	\node at (0,-0.4) {$\varphi$};
	\end{scope}
	\node at (0,0) {$\times$};
	\draw[ultra thick, black] (1,0) circle (0.5);
	\draw[->, black!70] (1,0) [partial ellipse=-120:-60:0.7 and 0.7];
	\node at (1,-1) {$t$};
	\end{tikzpicture}
	\caption{The choice of coordinates on $S^2\times S^1$, seen also in Paper \ref{lbl:paper2}.}\label{FIG:s2s1_coordinates}
\end{figure}


One can use the same theory as on flat space with the only change that we have to break the superconformal invariance.{\color{red}Change it. put that here the sc algebra is not present but the part we want is realized as spacetime isomenties the reason we can do it is that there are no conformal generators in the algebra} This means that from the supergenerators one can find a projection of them which reduces the amount from 16 to 8, since there are no conformal supercharges only the $Q_{\alpha a \dot a}$. Also the bosonic generators are reduced to $su(2)\oplus u(1)\oplus su(2)_R$.

The way to realise the $R$-symmetry part is by the need of a background R-symmetry connection which we will take to be $(A_H)^a{}_b \neq 0$. {\color{red} change to conformal case S2timesR and to compactify we need the Rsymmetry.} From the original R-symmetry algebra in the conformal case $su(2)_C\oplus su(2)_H$ we have broken $su(2)_H$ with the presence of the specific connection, hence the algebra reduces to $su(2) \oplus u(1)\oplus su(2)_C$.\footnote{Equivalently one can chooses to break the $su(2)_C$ using a background R-symmetry connection $(A_C)^{\dot{a}}{}_{\dot{b}}\neq 0$. The fixed locus would be different then.} The generators are $R_{\dot a \dot b}$ for the R-symmetry, $z$ for the $u(1)$ on the $S^1$ and $j_3$, $j_\pm$ for the $su(2)$ on the $S^2$. 



The supersymmetric part of the algebra is as follows
\begin{align}
\{ Q_{11\dot{a}}, Q_{12\dot{c}}\} &=-r^{-1} \epsilon_{\dot a \dot c } J_+\\
\{ Q_{21\dot{a}}, Q_{22\dot{c}}\} &=-r^{-1} \epsilon_{\dot a \dot c } J_-\\
\{ Q_{11\dot{a}}, Q_{22\dot{c}}\} &=-r^{-1} \epsilon_{\dot a \dot c } (i J_3+ \mathcal{Z}) - i r^{-1} R_{\dot a \dot c}\\
\{ Q_{21\dot{a}}, Q_{12\dot{c}}\} &=-r^{-1} \epsilon_{\dot a \dot c } (i J_3- \mathcal{Z}) - i r^{-1} R_{\dot a \dot c}
\end{align}
were we defined
\begin{equation}
J_\ast= - \mathcal{L}_{j_\ast}, \quad \text{and} \quad \mathcal Z = - \mathcal{L}_z
\end{equation}
as in Paper \ref{lbl:paper2}. Also the definitions for $R$ can be found in (4.17) of Paper \ref{lbl:paper2}.

We will now focus on the twisted subalgebra of 
\begin{equation}\label{eq:susycharges2s1}
\mathcal{Q} = \frac{i}{2} (\mathcal{Q}_1 + \mathcal{Q}_2)\, ,
\end{equation}
where $\mathcal{Q}_1=Q_{11\dot 2} + Q_{12\dot 1}$ and $\mathcal{Q}_2= Q_{21\dot 2}+Q_{22\dot 2}$. Indeed both $\mathcal Q_1$ and $\mathcal{Q}_2$ are nilpotent so any combination of these is equivalent. 
%Note that our choice of supercharge is different from Rastelli et al \cite{Beem:2013sza} in \eqref{centralcharge4d} or in 3d \cite{Chester:2014mea}, \eqref{commutatorqmath} since $\{\mathcal{Q}_1, \mathcal{Q}_2 \}=2ir^{-1} (J_3 - R_{\dot 1 \dot 2}) $. \footnote{The same choice was taken also by \cite{Dedushenko:2016jxl} since it is a more convenient choice when one takes the field theory approach.}. 
\begin{figure}[ht]
	\centering
	\begin{tikzpicture}[thick,scale=1,>=latex]
	\begin{scope}[xshift=-2.1cm]
	\path[use as bounding box] (-1.6,-1.9) rectangle (1.6,1.9);
	\colorlet{wilsonline}{cyan}
	\colorlet{shadow}{black!10}
	\begin{scope}
	\clip (-1.5,-1.5) rectangle (1.5,1.5);
	\fill [shadow] (0,0) circle[radius=1.5];
	\fill [white] (-0.1,0.5) circle[radius=1.7];
	\end{scope}
	\draw[->-=0.46, black!50] (0,1.25) [partial ellipse=-270:+90:0.57 and 0.25];
	\draw[->-=0.4, black!50] (0,0.97) [partial ellipse=-200:20:1.05 and 0.48];
	\draw[->-=0.39, black!50] (0,0.51) [partial ellipse=-190:10:1.39 and 0.6];
	\draw[->-=0.38, black!50] (0,0) [partial ellipse=-180:0:1.5 and 0.68];
	\draw[->-=0.38, black!50] (0,-0.52) [partial ellipse=-170:-10:1.39 and 0.63];
	\draw[ultra thick, black] (0,0) circle (1.5);
	\fill [black](0,1.35) ellipse (1.5pt and 1pt);
	\fill [black!30](0,-1.35) ellipse (1.5pt and 1pt);
	\end{scope}
	% \node at (0,0) {$\times$};
	% \draw[ultra thick, black] (1,0) circle (0.5);
	% \draw[->, black!70] (1,0) [partial ellipse=-120:-60:0.7 and 0.7];
	% \node at (1,-1) {$t$};
	\end{tikzpicture}
	\qquad
	\begin{tikzpicture}[thick,scale=1]
	\node at (-2.5,0) {$\longmapsto$};
	\colorlet{wilsonline}{cyan}
	\colorlet{shadow}{black!5}
	\begin{scope}
	\clip (-1.5,-1.5) rectangle (1.5,1.5);
	\fill [shadow] (0,0) circle[radius=1.5];
	\fill [white] (-0.1,0.5) circle[radius=1.7];
	\end{scope}
	\draw[->-=0.46, black!10] (0,1.25) [partial ellipse=-270:+90:0.57 and 0.25];
	\draw[->-=0.4, black!10] (0,0.97) [partial ellipse=-200:20:1.05 and 0.48];
	\draw[->-=0.39, black!10] (0,0.51) [partial ellipse=-190:10:1.39 and 0.6];
	\draw[->-=0.38, black!10] (0,0) [partial ellipse=-180:0:1.5 and 0.68];
	\draw[->-=0.38, black!10] (0,-0.52) [partial ellipse=-170:-10:1.39 and 0.63];
	\draw[ultra thick, black!20] (0,0) circle (1.5);
	\draw[ultra thick, dashed, red] (0,1.35) -- (0,-1.35);
	\fill [black](0,1.35) ellipse (1.5pt and 1pt);
	\fill [black](0,-1.35) ellipse (1.5pt and 1pt);
	\draw[ultra thick, black] (3.0, 1.0) ellipse (1.0 and 0.5);
	\draw[ultra thick, black] (3.0,-1.0) ellipse (1.0 and 0.5);
	\node at (3.6, 0.95) {$S^1_{\mathrm{N}}$};
	\node at (3.6,-0.95) {$S^1_{\mathrm{S}}$};
	\draw[green!50!black!50,->] (0.3, 1.6) to[out=25, in=145] (2.0, 1.4);
	\draw[green!50!black!50,->] (0.3,-1.6) to[out=-25, in=-145] (2.0,-1.4);
	\end{tikzpicture}
	\caption{The fixed point locus $S^2 \times S^1$ is the disjoint union of two copies of $S^1$ located at the poles of the two-sphere. (The figure is part of Paper \ref{lbl:paper2})}\label{Fig:s2s1loc}
\end{figure}

As in the previous case one has to find twisted operators that live in the fixed locus of $\mathcal Q ^2 \approx J_3 - R_{\dot 1 \dot 2}$. In the case of these operators being singlets under $su(2)_C$, the operators live in the fixed locus of $J_3\approx \mathcal{L}_{\partial_\phi}$. This contains the fixed locus on the $S^2$ which is two points, the north pole (NP) and the south pole (SP). The total locus on $S^2\times S^1$ is $\{NP\}\times S^1$ and $\{SP\}\times S^1$, see Figure \ref{Fig:s2s1loc}.


In the case of the $N=4$ hypermultiplet the operators are the same as in the case of the SCFT which are the scalars $q^a$. These are indeed singlets under $su(2)_C$ so they follow the above fixed locus discussion. 

One can find in Paper \ref{lbl:paper2} that on the two disjoint parts of the fixed locus the operators are
\begin{equation}\label{eq:bpsopss2s1}
q^{\pm}= u_{a}^\pm q^a = q^1 \pm q^2
\end{equation}
where plus and minus indicate north and south pole submanifolds respectively. Notice that the dressing factor takes the form $u_a^\pm = (1, \pm 1)$.{\color{red} reference before. also comment where is the origin?}

\chapter{Partial localization}\label{sec:partialloc}

Another way of realizing this procedure is using the localization technique. It was originally introduces in \cite{Dedushenko:2016jxl} and also it was used and adapted in other works too \cite{Pan:2017zie, Mezei:2018url, Pan:2019bor, Bonetti:2016nma, Costello:2018txb}. In Paper \ref{lbl:paper2} is was generalized for a large class of manifolds $\mathcal{M}$ for hypermultiplets. 

The general strategy is at follows. Let us assume $\mathcal M$ is a closed 3d manifold with metric $g$. Also consider a 3d theory with an $\mathcal N=4$ hypermultiplet $(q_a, \tilde q_a, \psi_{\dot a}, \tilde \psi_{\dot a})$, where the scalars $q, \tilde q$ belong to the (2,1) representation of $su(2)_H\oplus su(2)_C$  and the fermions to the (1,2). The R-symmetry currents are coupled to the background flat connections $A_H$ and $A_C$. The theory is coupled to a background vector multiplet with a connection $A$. We will pack all these connections to a covariant derivative $D= \nabla - i (A+ A_H + A_C)$. 

If the supercharge $Q_{a \dot a}$ is associated to a spinor $\xi^{a\dot a}$, where from now on we are going to omit the explicit dependence on the spinor index $\alpha$and treat objects as matrices. The spinor $\xi^{a\dot a}$ satisfies the conformal killing spinor equation for some spinor $\tilde\xi^{a\dot a}$. Then the supersymmetric transformations are of the form
\begin{align}
\delta q^a = \xi ^{a \dot a } \psi_{\dot a}, \quad \delta \tilde q^a = \xi ^{a \dot a } \tilde \psi_{\dot a},\\
\delta \psi_{\dot a }= i \gamma^\mu \xi_{a\dot a }D_\mu q ^a +i \tilde \xi_{a\dot a } q^a - i \xi_{a\dot c }\Phi_{\dot{a}}{}^{\dot c}q^a,- i \nu _{a\dot a } \\
\delta \tilde\psi_{\dot a }= i \gamma^\mu \xi_{a\dot a }D_\mu\tilde q ^a +i \tilde \xi_{a\dot a }\tilde q^a - i \xi_{a\dot c }\tilde q^a\Phi_{\dot{a}}{}^{\dot c} - i \nu _{a\dot a } \tilde G^a\\
\delta G^a = \nu ^{a\dot a } (\gamma^\mu D_\mu \psi_{dot a} - \Phi _{\dot a \dot c }\psi ^{\dot{c}})\\
\delta \tilde G^a = \nu ^{a\dot a } (\gamma^\mu D_\mu \tilde \psi_{dot a} - \Phi _{\dot a \dot c }\tilde\psi ^{\dot{c}})
\end{align}
where we have added two auxiliary fields $G,\tilde G$ together with their associated auxiliary spinors $\nu ^{a\dot a}$ in order to ensure off-shell closure of the supersymmetry\footnote{The auxiliary spinors satisfy some conditions mentioned in (2.15) of Paper \ref{lbl:paper2}}.

Finally the action is 
\begin{align}
&S= \int_{\mathcal{M}} \star \mathcal{L}, \quad \text{where}\\
\mathcal{L} = D^\mu \tilde q ^a D_\mu q_a - &i \tilde \psi^{\dot a} \gamma^\mu D_\mu \psi_{dot a} + R/8 \tilde q^a q_a+ \tilde G^a G_a\\ - \tilde q^a (i D_{ac} + &1/2 \epsilon_{ac} \Phi^{\dot a \dot c}\Phi_{\dot a \dot c})q^c + i \tilde \psi_{\dot a } \Phi^{\dot a \dot c }\psi_{\dot c }
\end{align}
{\color{red} say why it is not conformal, coupling to the vector field?}
Then the theory squares to the $\mathcal{L}_v$, where $v$ is the killing vector 
\begin{equation}
v^\mu= i \xi^{a\dot a }\gamma^\mu \xi_{a\dot a }
\end{equation} 
In particular $v$ satisfies $\mathcal{L}_v g =0$.

Let $M_v$ be the fixed locus manifold of $v$. Even though we are goin go work on the abelian vase for simplicity, the non Abelian version can be found in Paper \ref{lbl:paper2}. 

We are going to apply localization argument in Section \ref{Sec:generalloc}.  We introduce the equivariant differential $d_v = d-\iota_v$ and an equivariant closed polyform $\Omega$ such that $d_v \Omega =0$. This polyform has the following form 
\begin{equation}
\Omega = \ast\mathcal{L} + \alpha_1
\end{equation} 
where $\alpha_1$ is a 1-form and $\ast\mathcal L$ is a 3-form or a top form in three dimensions. This means that the following replace is valid
\begin{equation}\label{eq:partialloc}
\int_{\mathcal{M}} \ast \mathcal{L}\quad \to\quad \int_{\mathcal{M}} \Omega = \int_{\mathcal{M}_v} \frac{\iota^\ast \alpha_1}{e(N\mathcal{M}_v)}
\end{equation}

At the last step we use the localization formula from \eqref{eq:localizationformulasubmanifold}.

Hence the only thing remaining is to identify the 1-form $\alpha_1$. This was done in Paper \ref{lbl:paper2} using that $\Omega$ is equivariantly closed:
\begin{equation}
d_v (\ast \mathcal{L} +\alpha_1)=0 \to \iota_v (\ast \mathcal{L}) = d \alpha_1 \quad\text{and}\quad  \iota_v \alpha_1 = 0.
\end{equation}
so that 
\begin{equation}\label{eq:a1s2s1}
\alpha_{1\mu} =X_{ac} (\tilde q^a D_\mu q ^c - D_\mu\tilde q^a q ^c) + \tilde{q}^a q _a w_\mu + 2 (\Lambda_{ac})_\mu \tilde{q}^a q^c
\end{equation}
The various matrices that are defined here are functions of the killing spinor
\begin{align}
X_{ac}&= \xi_a^{\dot a}\xi_{c\dot a}, \quad w_\mu = \xi^{a \dot a }\gamma_\mu \tilde{\xi} _{a \dot a},\\
&(\Lambda_{ac})_\mu= (\xi_{a \dot a}\gamma _\mu \xi_{c\dot c })\Phi^{\dot a \dot c}\, .
\end{align}




\section{$S^2\times S^1$}

We are going to apply this prescription to the $S^2\times S^1 $ case that we discussed in Chapter \ref{section:s2s1}.  

As we said the choice of the supercharge is \eqref{eq:susycharges2s1} which means that the killing vector is along the equator of $S^2$, $v= - r^{-1}\partial_{\phi}$. The localizing locus is the disjoint union of $\mathcal{M}_v =( \{NP\}\times S^1 )\cup (\{SP\}\times S^1)$, which is also shown in Figure \ref{Fig:s2s1loc}. By using the localization procedure that we describe above one finds that on the north pole the localizing integral \eqref{eq:partialloc} simplifies to 
\begin{equation}
S_N= 2\pi r \oint_{S^1} \tilde q^+ (\partial_t - i \zeta)q^+
\end{equation}
using the definition of $\alpha_1$ and restricting on $\{NP\}\times S^1$. $\zeta$ here is a BPS combination of the vector multiplet fields $\zeta = a -i \beta \sigma$, where $a$ and $\sigma$ are the BPS connection and scalar field of the vector multiplet respectively. 

Similarly for the south pole we have 
\begin{equation}
S_S= -2\pi r \oint_{S^1} \tilde q^- (\partial_t - i \zeta^*)q^-.
\end{equation}
It is important to notice that we got exactly the BPS operators \eqref{eq:bpsopss2s1}. One can pack $\mathcal{D}_N= \partial_t - i \zeta$ and $\mathcal{D}_S= \partial_t - i \zeta^*$, which can be considered as twisted covariant derivatives. The final form of the action is
\begin{equation}\label{eq:finalactiontopqm}
S= 2\pi r \oint_{S^1} \tilde q^+ \mathcal{D}_N q^+ -2\pi r \oint_{S^1} \tilde q^- \mathcal{D}_S q^-.
\end{equation}
which has the form of two copies of topological quantum mechanics. 

In Paper \ref{lbl:paper2}, we study the path integral of such quantum mechanics by addressing the problem of identifying the correct integration cycle across the lines of \cite{Witten:2010zr} and by showing that its partition function precisely matches its three-dimensional counterpart. As a result a mapping between 3d $\mathcal N=4$ theories and 1d topological quantum mechanics of protected operators was established.
{\color{red}expand topological quantum mechanics and why?  Show the partition function.}