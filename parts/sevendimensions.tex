\part{$N=1$ super Yang Mills in 7d on Sasaki-Einstein manifolds}\label{part:7d}

There are many exact results for partition functions that have been produced in diverse dimensions considering many different geometries. However the even and odd dimensions are treated differently. Here we are going to summarise the main way of deriving the partition function for odd dimensions using the example of $N=1$ super Yang Mills in 7d on Sasaki Einstein manifolds from Paper \ref{lbl:paper1}, which was first considered in \cite{Minahan:2015jta}. This example is a very good toy model since it helps understanding how the geometric structure of the theory can translate in the structure of the result of the partition function.

We will start by explaining what instanton contribution we are going to stumble upon during localization. The contact instanton in 7d is going to be explained in parallel with the usual 4d instanton. Then we are going to give a motivation on the fact that we consider supersymmetric theories on Sasaki-Einstein manifolds by studying on which 7d manifolds we can place supersymmetric theories. Afterwards we introduce $N=1$ super Yang Mills in 7d on Sasaki Einstein manifolds. We will use the procedure described above and the localization technique described in Section \ref{Sec:generalloc} in order to calculate the partition function for such theories. Finally we are going to attempt to explain the geometrical structure of the result and how it can be interpreted using fibrations. 

\chapter{Instantons}\label{sec:instantons}

\section{4d case}
Let us first discuss the case of Yang-Mills on a 4-dimensional manifold $M$, introduced in \cite{Yang:1954ek}, and then generalize it. In this discussion we will try to use some of the concepts of Section \ref{sec:symplecticgeometry}. \footnote{We assume a general semi-simple Group $G$ but we will not use it explicitly and we will omit to write explicitly the trace over the Lie algebra $\mathfrak{g}$ in the integrals.} 

[citation for instanton?]

The Yang-Mills action takes the form
\begin{equation}\label{eq:4dymaction}
S= \int F\wedge \ast F=\frac{1}{2} \int d^4 x F_{\mu \nu}F^{\mu\nu}\, ,
\end{equation}
 where $\ast F$ is the Hodge dual form of the Lie algebra valued 2-form $F$. The Euler-Lagrange equation and the Bianchi identity take the form 
 \begin{align}\
 D_{\mu}F^{\mu\nu}= 0 \to d_A \ast F=0\, ,\\
 D_{[\mu}F_{\nu\rho]}=0\to d_A F=0\, ,
 \end{align} 
with $D_\mu= \partial_\mu + [A_\mu,\cdot] $ the covariant derivative with respect to the gauge field $A_\mu$. This covariant derivative can be rewritten to the de Rham differential twisted by the gauge field $d_A$ acting on a form $\omega \in \Omega^k(M)$ as $d_A \omega= d\omega+ [A, \omega]$. 

In 4-dimensions one can define the projectors $P^\pm$ that act on 2-forms
\begin{equation}\label{eq:4dprojector}
P^{\pm}= \frac{1}{2}(1\pm \ast)\, .
\end{equation}
As projectors, they satisfy $(P^\pm)^2= P^\pm$ and also $P^- = 1- P^+$. Using these projectors one can decompose 2-forms $\Omega^2 (M)= \Omega^{2+}(M)\oplus \Omega^{2-}(M)$, for example the field strength becomes
\begin{equation}\label{eq:4dFdecomposition}
F= F^+ + F^-, \quad \text{where}\quad \ast F^\pm = \pm F^\pm\, .
\end{equation}
These are the self-dual and anti-self-dual components of the field strength.

We can use this decomposition in the action \eqref{eq:4dymaction}
\begin{equation}
S=\int F^+ \wedge \ast F^+ + \int F^- \wedge \ast F^-\, .
\end{equation}
Thus we have the following bound
\begin{equation}
S\geq  \bigg|\int F^+ \wedge \ast F^+ - \int F^- \wedge \ast F^- \bigg|=\bigg|\int F\wedge F\bigg|\, ,
\end{equation}
where we substituted the Hodge-dual fields according to \eqref{eq:4dFdecomposition}. This lower bound is saturated only if $F$ is self- or anti-self-dual,i.e $F=F^+$ or $F=F^-$. 
This solutions can be summarized in 
\begin{equation}
F=\pm \ast F\, ,
\end{equation} 
which is called instanton equations and its solutions, depending on the plus or minus sign, are called instantons or anti-instantons respectively.
This bound is also interesting since it involves a topological invariant. The integral 
\begin{equation}
c_2 = -\frac{1}{8\pi^2} \int F\wedge F
\end{equation}
is the \emph{instanton number}. It also constains the second Chern class which is a topological invariant quantity. [cite?]





\section{7d case}\label{sec:contactinstanton}
In this case we are going to discuss the Yang Mills action in the context of contact geometry, which we introduced in Section \ref{sec:constactgeometry}.

We will start again with the same action as before on a smooth 7-dimensional manifold $M$
\begin{equation}
S=\int F\wedge \ast F\, ,
\end{equation}
where the fields are again Lie algebra valued which we omit here for optical reasons. The field strength $F$ is a 2-form.
As it was discussed in Section \ref{sec:constactgeometry} one can use the contact form $\kappa$ and the Reeb vector $R$ associated to it,to define the projectors \eqref{eq:contactprojectors} that decompose forms into a vertical and a horizontal part. 

We can decompose the horizontal part even more if we restrict ourselves in 2-forms for the purpose of the instanton equation. Indeed, as it was explained in Paper \ref{lbl:paper1}, one can define the projector
\begin{equation}
\check{P}F= \frac{1}{12}[\ast (F \wedge \ast d\kappa)]d\kappa
\end{equation}
in order to extract the part which is proportional to $d\kappa$. Then the 2-form $F$ becomes
\begin{equation}
 F=\hat{F}+ \check F=\hat{F}+ \frac{1}{24}\tilde F d\kappa
\end{equation}
where $\hat F _{\mu\nu}(d\kappa)^{\mu\nu}=0$ and $\tilde F = F_{\mu\nu}(d\kappa)^{\mu\nu}$. Similarly to the case before, the horizontal part of $\hat{F}$ can be decomposed using the projector,
\begin{equation}
P^\pm F =\frac{1}{2}(F\pm \frac{1}{2}\iota_R \ast(d\kappa\wedge F))
\end{equation}
then the decomposition of $F$ becomes the following
\begin{equation}\label{eq:7ddecomposition}
F=F_V + F_H =F_V +\hat{F}^+_H + \hat{F}^+_H +\check F_H.
\end{equation}
where the last term can be also written as $\check F_H=\frac{1}{24} \tilde F d\kappa.$

The Yang-Mills action can be decomposed as follows using the above decomposition, \eqref{eq:7ddecomposition}
\begin{equation}
S= \int F_V \wedge \ast F_V + \int \hat F^+_H\wedge \ast \hat F^-_H +  \int \hat F^-_H\wedge \ast \hat F^-_H +  \int \check F_H\wedge \ast \check F_H
\end{equation}
The bound can be obtained as follows
\begin{align}
S\geq \int \hat F^+_H\wedge \ast \hat F^-_H +  \int \hat F^-_H\wedge \ast \hat F^-_H \implies\\
 S\geq  \bigg|\int \hat F^+_H\wedge \ast \hat F^-_H -  \int \hat F^-_H\wedge \ast \hat F^-_H \bigg|= \frac{1}{2}\bigg|\int \kappa \wedge d \kappa \wedge \hat F \wedge \hat F \bigg|
\end{align}
The first inequality is satisfied in the case where $F_V = 0$ and $\check{F}_H=0$.  The second one in the case of $\hat{F}^+_H =0$ or $\hat{F}^-_H =0$. One can combine all these restrictions into one equation which is referred to as "contact instanton" equation in 7 dimensions
\begin{equation}\label{eq:contactinstanton}
\ast F = \pm \frac{1}{2}\kappa \wedge d \kappa \wedge F
\end{equation}
It is also important to note that the contact instantons and anti instantons are automatically solutions of the Yang-Mills equation since 
\begin{equation}
d_A \ast F = \frac{1}{2}\pm d\kappa \wedge d \kappa \wedge F =0\, ,
\end{equation}
due to the orthogonality of $\hat F _H$ --~the solutions of the instanton equation~-- and $d\kappa$.

It is worth mentioning that the instanton equation was first written in the case of five dimensions in \cite{Kallen:2012cs} and further studied in \cite{Baraglia}.  




\chapter{Killing spinors}\label{sec:killingspinorsgeneral}

From now on we will restrict ourselves in the case of 7d manifolds since this is the topic of Paper \ref{lbl:paper1}.
This section is giving some insight why in Paper \ref{lbl:paper1} we restrict ourselves to Sasaki-Einstein manifolds.

We will revise some facts about Killing spinors and we refer the reader to \cite{Blau,BoyerGalicki,Bar}.
Let us start from a n-dimensional Riemannian manifold ($M,g$) which admits a spin structure. A spinor $\zeta$ is Killing if there exist a constant $\alpha\in \mathbb{C}$ such that for all tangent vectors $X$,
\begin{equation}\label{eq:killingspinoreqgeneral}
\nabla _X \zeta = \alpha X\cdot \zeta
\end{equation} 
is satisfied. Here $\nabla$ denotes the covariantized derivative with respect to the spin connection and $X\cdot \zeta$ the Clifford multiplication. In the case of $\alpha\in \mathbb{R}$ then $\zeta$ is called real Killing spinor. 

If there exist a Killing spinor on $M$ then the manifold is Einstein with Ricci scalar 
\begin{equation}
R= -4n (n-1)\alpha^2\, .
\end{equation}
If we restrict our attention to positive curvature then for a given manifold there are two possible choices of $\alpha$. When these are put back into the Killing spinor equation \eqref{eq:killingspinoreqgeneral} there is a specific amount of independent solutions of the Killing spinors $\zeta$. For supersymmetric theories (and for localization) we need at least one Killing spinor. 

In the case of 7-dimensional manifolds with positive curvature the ones that admit Killing spinors were classified by \cite{Bar}.  More specifically the 7-dimensional complete simply connected Riemannian spin manifolds with positive curvature that admit a non-trivial Killing spinor are
\begin{itemize}
	\item The 7-sphere $S^7$ with 16 Killing spinors,
	\item 3-Sasaki manifolds with 3 Killing spinors,
	\item Sasaki-Einstein manifolds with 2 Killing spinors and
	\item Proper $G_2$-manifolds with 1 Killing spinor,
\end{itemize} 
as was also explained in Paper \ref{lbl:paper1}.
In this thesis we are going to mainly focus on the first and third cases, whereas in Paper \ref{lbl:paper1}, there is also a small discussion about the 3-Sasaki manifolds.

The second and last cases are out of the scope of this thesis, however we can say a few words about what these manifolds are.

A 3-Sasaki manifold is a Sasakian manifold but there exist three Sasaki structures $\{R_a, \kappa_a, J_a\}$ which have an $su(2)$ structure
\begin{align}
\iota_{R_a}\kappa_b = \delta_{ab}\, ,\\
[R_a,R_b]=\epsilon_{abc}R^c\, .
\end{align}
It is important to note that the cone over a 3-Sasaki manifold is a hyperkähler manifold which has three complex structures. Localization results for the 3-Sasaki manifolds can be found partially in Paper \ref{lbl:paper1} but for a more in depth work one can look into \cite{Rocen:2018xwo} and  \cite{Iakovidis:2020znp}.

The proper $G_2$ manifold is one that admits a 3-form $\Phi$ associated to the $G_2$ structure, which satisfies $d\Phi=-8 \lambda (\ast \Phi)$ for some $\lambda\neq 0$. For more information about such manifolds we refer the reader to \cite{Friedrich:1997}. Also there has been developments with adding supersymmetric theories to them, for example \cite{Acharya:1997jn} and \cite{Prins:2018hjc}. It is interesting that one can find a minimisation of the action on something that is called "$G^2$-instanton", which is different that the contact instanton in \eqref{eq:contactinstanton} since these manifolds do not have a contact structure. The instanton equation then is $\ast F = \Phi\wedge F$. These manifolds are not yet discussed in the context of localization due to the lack of contact structure which is heavily used in these kind of calculations. 


\chapter{7 Dimensions and localization}\label{sec:superymin7d}
Since we motivated why we are interested in 7-dimensional Sasaki-Einstein manifolds and the existence of Killing spinors on them, we can proceed with placing supersymmetric theories on such manifolds.

We will follow mainly Paper \ref{lbl:paper1} and \cite{Minahan:2015jta}. The starting point is $\mathcal{N}=1$ super-Yang Mills on 10 dimensions $\mathbb{R}^{9,1}$. The theory contains a gauge field $A_{M}$, $M=0,\ldots,9$ with its field strength and a Majorana-Weyl fermion $\Psi_\alpha$, with $\alpha=1,\ldots,16$ transforming under the adjoint representation of the gauge group $G$. Finally $\Gamma_{M}$ denote the 10-dimensional Dirac matrices.
The 10-dimensional action, \cite{BrinkSS} is
\begin{equation}
S_{10}= \frac{1}{g^2_{10}} \int d^{10}x \text{Tr}(\frac{1}{2}F^{MN}F_{MN}- \Psi \Gamma^M D_M \Psi)\, ,
\end{equation}
where $D_M$ is the 10 dimensional covariant derivative with respect to the gauge field. This action is invariant under the supersymmetry transformations
\begin{align}
\delta A_M = \epsilon\Gamma_M \Psi\, ,\\
\delta \Psi = \frac{1}{2}\Gamma^{MN}F_{MN}\epsilon\, .
\end{align} 
where the supersymmetric parameter $\epsilon$ is a constant Majorana-Weyl spinor.

We are going to dimensionally reduce in order to get a supersymmetric theory in a lower dimensional curved manifold. This reduction is called Scherk-Schwart reduction and it was done by \cite{Blau} for on-shell supersymmetry and by \cite{Berkovits:1993hx} and \cite{Fujitsuka:2012wg} in the off-shell version.


We separate the gauge field into the 7-dimensional one $A_\mu$, $\mu=1\ldots7$ and in the compactified directions the fields become scalars $\phi_A$, $A= 0,8,9$. Also the derivative on the compactifying dimensions vanishes.

In order to have consistent supersymmetry on the 7-dimensional manifold, the 10-dimensional Majorana-Weyl spinor needs to satisfy a generalized Killing spinor equation that has similar form with \eqref{eq:killingspinoreqgeneral}
\begin{equation} \label{killcond}
\nabla_\mu \epsilon  =  \frac{1}{2r} \tilde\Gamma_\mu \Lambda \epsilon\, ,
\end{equation}
where $\Lambda = \Gamma^8 \tilde\Gamma^9 \Gamma^0$ and $r$ is a dimensionful parameter corresponding to the size of the manifold. We will drop the $r$ for convenience. It can be restored at any point using dimensional analysis. As a result the 7-dimensional manifold has to be one of the manifolds discussed in Section \ref{sec:killingspinorsgeneral}.

In order to perform localization we need off-shell supersymmetry hence we also need to introduce an auxiliary field $K^m$, $m=1\ldots7$ and the bosonic pure spinor $\nu_m$ associated to it. 
We are going to review the theory however we refer the reader to Paper \ref{lbl:paper1} for more details. 




Following \cite{Pestun:2007rz,Minahan:2015jta} and Paper \ref{lbl:paper1}, the pure spinors $\nu_m$ satisfy the relations
\begin{align} \label{purespinordef}
\epsilon\Gamma^M \nu_m &=0  ,\\
\nu_m \Gamma^M \nu_n &= \delta_{mn} v^M ,\\
\nu^m_\alpha \nu^m_\beta + \epsilon_\alpha \epsilon_\beta &= \frac{1}{2} v^M \tilde \Gamma_{M\alpha\beta} \nn\, .
\end{align}
Note that these relations only determine the $\nu$'s up to an internal $SO(7)$ symmetry. 

Here $v^M$ denotes the vector field 
\begin{equation} \label{eq:vdef}
v^M = \epsilon \Gamma^M \epsilon\, ,
\end{equation}
which is a Killing vector.
For the Sasaki-Einstein case, which is the case we are considering here, we are free to choose $v^0=1$ and $v^8=v^9=0$ resulting in $v^\mu v_\mu=1$.\footnote{For useful identities containing the $\Gamma$ matrices we refer the reader to Appendix A of Paper \ref{lbl:paper1}.}

The off-shell supersymmetric transformations read
\begin{align} \label{susytransoff}
&\delta  A_M = \epsilon \Gamma _M \Psi \nn\, ,\\
&\delta \Psi = \frac{1}{2} F_{MN}\Gamma ^{MN}\epsilon + \tfrac{8}{7} \Gamma^{\mu B} \phi _B \nabla _\mu \epsilon + K^m \nu_m\, , \\
&\delta K^m =  -\nu^m \Gamma^M D_M \Psi + \tfrac32 \nu^m \Lambda \Psi \nn\, .
\end{align}

It can be shown, see \cite{Pestun:2007rz,Minahan:2015jta} or Paper \ref{lbl:paper1}, that this off-shell supersymmetry squares to symmetries of the theory. Schematically this reads
\begin{equation}
\delta^2 = -\mathcal{L} - G - R - S\, ,
\end{equation}
where $\mathcal L$ is a Lie derivative along the vector field $v$, $G$ is a gauge transformation, $R$ is the $R$-symmetry and $S$ is the $SO(7)$ rotations of the auxiliary fields $K^m$.

The reduced action becomes

\begin{align} \label{7DactionOffShell}
S= \frac{1}{g_{7D}^2} \int d^{7}x \sqrt{-g} \text{Tr} \Big( 
\frac{1}{2}& F^{MN}F_{MN} 
- \Psi \Gamma^M D_M \Psi 
+8 \phi^A \phi_A
+\tfrac32 \Psi \Lambda \Psi \notag \\
&-2 [\phi^A,\phi^B]\phi^C \varepsilon_{ABC}
- K^m K_m
\Big)\, .
\end{align}


Nonetheless one can simplify the transformations by a redefinition of fields which is has a more clear geometric interpretation and use. Thus we are going to map these fields to differential forms. One can start by identifying as Reeb vector $R^\mu=v^\mu$ from \eqref{eq:vdef} and as contact form $\kappa_\mu = g_{\mu\nu}R^\nu$. Note that the condition  $v^\mu v_\mu=1$ corresponds to the definition of the Reeb vector \eqref{eq:defcontactreeb}. \footnote{In Paper \ref{lbl:paper1} all the components of the contact structure are identified.}

One can make the following redefinitions of the fermionic degrees of freedom 
\begin{align}
\psi_{\mu}& = \epsilon \Gamma_{\mu}\Psi\, ,\\
\Upsilon_{\mu\nu }&= (\nu_m \Gamma^0 \Psi)(\nu^\mu \Gamma_{\mu\nu 0} \epsilon)
\end{align}
and of the bosonic fields
\begin{align}
\Phi_{\mu\nu\lambda }&= \frac{1}{2}\phi_A(\epsilon \Gamma_{\mu\nu\lambda} \Gamma^{A0}\epsilon)\, ,\\
\phi_0 &= \sigma\, .
\end{align}
It is important to note that $\phi_0$ is treated differently since it comes from compactifying the time-like direction and thought it is a tachionic degree of freedom..

Additional to these redefinitions we also define $\eta= \frac{1}{2}\epsilon \Gamma_{A}\Psi(\epsilon \Gamma_{\mu\nu\lambda} \Gamma^{A0}\epsilon)$ which is the superpartner of $\Phi$ and $H_\mu=(\nu^m \Gamma_{\mu\nu 0} \epsilon)(K_m + \ldots)$ which is the superpartner of $\Upsilon$. 

The result is the cohomological complex 
\begin{align}
&\delta A = \psi\, , \\ 
&\delta \psi = -\iota_R F + i G_{\sigma}A \, ,\\
&\delta \sigma = i \iota_R \psi \, ,\\
&\delta \Phi = \eta \, ,\\
&\delta \eta = -\mathcal L_R^A \Phi + iG_{\sigma}\Phi \, , \\
&\delta \Upsilon = H\, , \\
&\delta H = -\mathcal L_R^A \Upsilon + iG_{\sigma}\Upsilon\, ,
\end{align}
where $d_A$ is the de Rham differential coupled to the connection $A$ and $\mathcal L_R^A$ is the corresponding Lie derivative along the Reeb vector field covariantized with respect to the connection $A$, $\mathcal L_R^A = \iota_R d_A + d_A \iota_R$. The gauge transformation $G_\sigma$ is given by $G_{\sigma} A = d_A {\sigma}$ on the gauge field and $G_{\sigma} = -[\sigma,\cdot]$ on the remaining fields.

It is very important to point out that the fields above are now differential forms and can be also decomposed using projectors. In Table \ref{table:cohomologicalcomplex} one can find a reminder of the field in the cohomological complex together with their superparners. 
Note that in Table \ref{table:cohomologicalcomplex} we have decomposed the 2-forms using the decomposition \eqref{eq:contactfulldecompostition}.

\renewcommand{\arraystretch}{1.4}
\begin{table}[h!]
	\begin{center}
		\begin{tabular}{|c|c|}
			\hline
			\multicolumn{1}{|c|}{Bosons}& \multicolumn{1}{c|}{Fermions}\\
			\hline
			$A$ ~~~{\rm connection} & $\psi\in \Omega^1 $ \\ \hline
			$H\in\Omega_H^{(2,0)}\oplus \Omega_H^{(0,2)}\oplus \Omega^0 d\kappa$ & $\Upsilon\in \Omega_H^{(2,0)}\oplus \Omega_H^{(0,2)}\oplus \Omega^0 d\kappa $ \\ \hline
			$\Phi\in\Omega_H^{(3,0)}\oplus \Omega_H^{(0,3)} $ & $\eta\in \Omega_H^{ (3,0)}\oplus \Omega_H^{(0,3)} $ \\ \hline
			$\sigma \in \Omega^0 $& \multicolumn{1}{c|}{} \\ \hline
		\end{tabular}
		\caption{The bosonic and fermionic fields of the cohomological complex. $(X,X')$-pairs of bosons and fermions appearing in the transformations \eqref{cohomoltranf1} are written on the same line. Note that we have suppressed the Lie algebra dependence. This table is part of a table of the map in Paper \ref{lbl:paper1}.}\label{table:cohomologicalcomplex}
\end{center}\end{table}

Redefining the field $\sigma \rightarrow -\sigma + i\iota_R A$, the above transformations can be written in the compact form
\begin{align}\label{cohomoltranf1}
&\delta X = X'\, ,\nn\\
&\delta X' = -\mathcal L_R X -iG_{\sigma}X \, ,\\
&\delta \sigma = 0\, ,\nn
\end{align}
where the $(X,X')$ pairs are given by $(A,\psi)$, $(H,\Upsilon)$ and $(\Phi,\eta)$. We see that $\delta^2 = -\mathcal L_R -iG_{\sigma}$, which is symmetries of the theory and this is the operator that is going to be used in localization in \eqref{eq:generallocgauge}.

Off-shellness is not the only requirement in order to get a correct localization result following Section \ref{sec:localizationgaugetheories}, one need to gauge fix to theory by introducing the Faddeev-Popov ghosts $c,\, \bar c$ the Lagrange multiplier $b$ and the zero modes $(a_0, \bar a _0, b_0)$ and $(c_0, \bar c_0)$ which are bosonic and fermionic respectively. We also introduce the standard BRST transformation \cite{Pestun:2007rz} $\delta_B$ which we combine with the supersymmetry into a new transformation $Q= \delta+\delta_B$, which squares to $Q^2 = -\mathcal L_R + i G_{a_0}$. The field transformations have the same form as in $\eqref{cohomoltranf1}$ with the replacement $\sigma \to a_0$.




\section{Localization technique}\label{sec:7dlocalizationpart}

In order to perform localization one has to find first what is the localization locus. This reads, \cite{Minahan:2017wkz} and Paper \ref{lbl:paper1}
\begin{align}
\iota_R F & = 0  \, ,\\
%\iota_R \Phi &= 0 \, , \\
\iota_R d_A \Phi &= 0 \, , \\
d_A \sigma &=0 \, ,\\
\hat{F}_H^- &= -d^\dagger_A \Phi\, ,\\
\check{F}_H \wedge d\kappa \wedge d\kappa &= 4 [\Phi^{-}, \Phi^+] \, .
\end{align}
As one can see in the case where $\Phi=0$ \footnote{One can also set $\sigma =0$ however that is not necessary since they are decoupled.} the localization locus is equivalent with the contact instanton $\hat F^+$ what we discussed in Chapter \ref{sec:contactinstanton}.

Also the action \eqref{7DactionOffShell} takes the form on the BPS locus
\begin{align}
S_{f.p.} = \frac{1}{g^2_7} \left[ \int 24\, V_7 \text{Tr}(\sigma^2)  + \frac{1}{2} \int \text{Tr} \left( \kappa \wedge d\kappa \wedge F \wedge F \right) \right]  \, ,
\end{align}
where $V_7$ denotes the volume form with respect to the metric $g$. One can see that the second term gives a contribution from the contact instanton. In practice the instanton contribution is complicated so we will continue our discussion by concentrating on only contributions of flat connections as {\color{red}ref all people that do that and reference instanton refers to}.

The localization term that is added is of the usual form \eqref{eq:generallocgauge} which results to the following full perturbative partition function 

\begin{equation} \label{partfun1}
{\small\int\limits_g d\sigma e^{- \frac{24}{g^2_7} V_7 \text{Tr}(\sigma^2)} \frac{ \overbrace{\sqrt{\text{det}_{\Omega_H^{(2,0)}}(S)\text{det}_{\Omega_H^{(0,2)}}(S) \text{det}_{\Omega^{0}}(S)}}^{\Upsilon}\overbrace{\sqrt{\text{det}_{\Omega^{0}}(S)}}^{c}
	\overbrace{\sqrt{\text{det}_{\Omega^{0}}(S)}}^{\bar{c}} }{
	\underbrace{\sqrt{\text{det}_{\Omega^1}(S)}}_{A}
	\underbrace{\sqrt{\text{det}_{\Omega_H^{(3,0)}}(S)\text{det}_{\Omega_H^{(0,3)}}(S)}}_{\Phi}
	\underbrace{\sqrt{\text{det}_{H^0}(S)}}_{b_0}
	\underbrace{\sqrt{\text{det}_{H^0}(S)}}_{\bar{a}_0}}},
\end{equation}
where we have replaced $Q^2=S$.

Here we show which fields contribute and in which form as in Paper \ref{lbl:paper1}. Also $H^0$ refers to harmonic 0-forms and since they are constant on a compact manifold we can discard them (using they stay unchanged under $Q^2$). By ignoring phases and decomposing all forms the result  after cancellations is 
\begin{equation} \label{partfun2}
Z
= \int\limits_g d\sigma e^{-\frac{24}{g^2_7} V_7 \text{Tr}(\sigma^2)}  det_{adj}^{'}\, sdet_{\Omega_H^{(0,\bullet)}}(-\mathcal L_R + i G_{a_0})\, ,
\end{equation}
where the $det_{adj}^{'}$ is the determinant over the adjoint representation of the Lie algebra which was implied in the previous steps. Note that as in \eqref{partfun1} or \eqref{eq:generallocgauge}, the superdeterminant means that the even forms are as numerators and the odd as denominators.The next step is to calculate that superdeterminant.

In the case of Sasaki-Einstein manifolds with metric cone $C(X)$, it was first pointed out by Schmude in \cite{Schmude:2014lfa}, the superdeterminant is obtained by counting holomorphic function on the metric cone, see also \cite{Schmude:2013dua,Eager:2012hx}. 

 More specifically on the horizontal space, as it was described in \cite{Sparks:2010sn}, due to the complex structure we can define Dolbeault operators, especially $\bar \partial_H$ which gives rise to the Kohn-Rossi cohomology groups, $H^{p,q}_{KR}$, \cite{KohnRossi}. 
 
 Let us look at the part of determinant that does not contain the gauge transformation. Since the Lie derivative $\mathcal{L}_R$ commutes with $\bar \partial_H$ the superdeterminant can be evaluated over $H^{0,\bullet}_{KR}$ instead. 
 In a similar fashion as in \cite{Qiu:2014oqa} there is a pairing between $H^{0,1}_{KR}$ and $H^{0,2}_{KR}$ but since the second one is zero from simple connectness both vanish.
 Additionally there is a pairing between the (0,0)-forms and the (0,3)-forms and since it was argued in \cite{Qiu:2016dyj} the same exist for the Kohn-Rossi cohomology. As a result the only factor remaining is the one with $H^{0,0}_{KR}\equiv H^0(C(M))$ which is the same as counting holomorphic functions on the moment map cone which we discussed in Chapter \ref{sec:toricsasakieinsteinintro}.
 
Ignoring the Lie algebra part for the moment and using the 1-Gorenstein condition, $\vec{\xi} \cdot \vec{u}_i = 1$,\cite{Sparks:2010sn}, we can write our superdeterminant in terms of the moment map cone:
\begin{align}\label{superdeterminant}
sdet_{\Omega_H^{(0,\bullet)}}(-\mathcal L_R + x) &= \frac{\prod \limits_{\vec{n}\in C_\mu(X) \cap \mathbb Z ^4 \setminus \{\vec{0}\}}\left( \vec{n} \cdot \vec{R} + x \right) } {\prod \limits_{\vec{n}\in C^\circ_\mu(X) \cap \mathbb Z^4}\left( \vec{n} \cdot \vec{R} - x \right)}\, ,
\end{align}
where $C^\circ_\mu(X)$ denotes the interior of the moment map cone. Here we also absorbed the minus signs in the denominator since they do not matter once you regularise.


As a result the final action takes the form 
\begin{align}\label{eq:fullgeneralpartitionfunction7d}
Z^{\text{pert}}_X&=\int\limits_t d \sigma\,  e^{-\frac{24}{g^2_7} V_7 \text{Tr}(\sigma^2)} \prod_\beta   i \langle \sigma, \beta \rangle \frac{\prod \limits_{\vec{n}\in C_\mu(X) \cap \mathbb Z^4 \setminus \{\vec{0}\}}\left( \vec{n} \cdot \vec{R} +  i \langle \sigma, \beta \rangle \right) } {\prod \limits_{\vec{n}\in C^\circ_\mu(X) \cap \mathbb Z^4}\left( \vec{n} \cdot \vec{R} -  i \langle \sigma, \beta \rangle \right)} \nonumber\\
&= \int\limits_t d \sigma \,  e^{-\frac{24}{g^2_7} V_7 \text{Tr}(\sigma^2)} \prod_\beta S_4^{C_\mu(X)}( i \langle \sigma, \beta \rangle |\vec{R})\, , 
\end{align}
where
\begin{equation}
S_4^{C_\mu(X)} (x|\vec{R}) = \frac{\prod \limits_{\vec{n}\in C_\mu(X) \cap \mathbb Z^4}\left( \vec{n} \cdot \vec{R} +  x \right) } {\prod \limits_{\vec{n}\in C^\circ_\mu(X) \cap \mathbb Z^4}\left( \vec{n} \cdot \vec{R} -  x \right)} \, .
\end{equation}
This function $S_4^{C_\mu(X)}$ is known as the generalised quadruple sine function associated to the cone $C_\mu(X)$, \cite{Winding:2016wpw,Narukawa}, see Paper \ref{lbl:paper1} in Appendix C for more information on the multiple sine functions. Note that here $\beta$ is the non-zero root of the Lie algebra $\mathfrak g$ and $t$ the Cartan subalgebra. 


\section{$S^7$ and factorization}

Let us decode the partition function \eqref{eq:fullgeneralpartitionfunction7d} by going to the example of $S^7$. Let $T^4$ be the torus action that acts as in \eqref{eq:torusactiontn}. The cone over $S^7$ is simply $\mathbb C^4$ and the moment map cone will have the form similar to \eqref{eq:cnmomentmap}

\begin{align}
&\mu : \mathbb{C}^4 \to \mathbb{R}^4_{\geq 0}\\
\mu=&\frac{1}{2}(|z_1|^2, |z_2|^2 , |z_3|^2  ,|z_4|^2). 
\end{align}


The polytope is restricted to $\sum_i |z_i|^2 =1$ which is a pyramid as it is shown in Figures \ref{figure2} and \ref{figure3}. The holomorphic functions on $\mathbb C^4$ correspond to the lattice points in the moment map. 

We will denote the lattice points $\vec n = (n_1,n_2,n_3,n_4)$ where $n_i \in Z_{\geq 0}$ since the moment map cone is $C_\mu =\mathbb{R}$. Its interior is $C_\mu^\circ =\mathbb{R}_{\geq 0}$. If we associate $e^i$ to be the basis vector associated to each $U(1)$ then the associated Reeb vector is of the form $R=\omega_i e^i$ on the moment map cone. In the case of the round sphere $\omega_i =1$ however we are more interested in the squashed sphere form now on. 
Then $\vec{n}\cdot \vec{R}= \omega_i n^i$, which is the factor that enters the superdeterminant. 

This means that the generalized quadruple sine takes the form 
where
\begin{equation}
S_4 (x|\vec{R}) = \frac{\prod\limits_{n_1,n_2,n_3,n_4 \geq 0}\left( n_1\omega_1+n_2\omega_2+n_3\omega_3+n_4\omega_4 + x \right) } {\prod\limits_{n_1,n_2,n_3,n_4 \geq 1}\left( n_1\omega_1+n_2\omega_2+n_3\omega_3+n_4\omega_4 - x \right)} \, ,
\end{equation}
which is the ordinary quadruple sine function.

This result encodes part of the geometry which is what we are going to illustrate in the following according to Paper \ref{lbl:paper1}. 
The $S^7$ has two different Hopf fibrations which can be found in Figures \ref{figure2} and \ref{figure3}.

Let us start with the first $S^1$ fibration. 
\begin{figure}[h!]
	\centering
	\hspace{1.5cm}
	\begin{tikzcd}
		S^7  \arrow[d] & S^1 \arrow[l] \\
		\mathbb C P ^3   & 
	\end{tikzcd}
\quad \quad \quad \quad
	\tdplotsetmaincoords{60}{65}
	\begin{tikzpicture}[tdplot_main_coords, scale=3,baseline={([yshift=-0.5ex]current bounding box.center)}]
	\draw[thick] (0.57735,0,0)--(-0.288675,0.5,0)--(-0.288675,-0.5,0)--cycle;
	\draw[thick] (0.57735,0,0)--(-0.288675,0.5,0)--(0,0,0.816497)--cycle;
	\draw[thick] (0.57735,0,0)--(-0.288675,-0.5,0)--(0,0,0.816497)--cycle;
	\draw[thick] (-0.288675,0.5,0)--(-0.288675,-0.5,0)--(0,0,0.816497)--cycle;
	\fill[draw=blue, fill=blue, fill opacity=0.2] (0, 0,0.204124)--(0.144338,0.25,0)--(0, 0, 0)--cycle;
	\fill[draw=blue, fill=blue, fill opacity=0.2] (0, 0,0.204124)--(0.144338,0.25,0)--(0.096225, 0.166667, 0.272166)--cycle;
	\fill[draw=blue, fill=blue, fill opacity=0.2] (0, 0,0.204124)--(-0.144338, 0.25, 0.408248)--(0.096225, 0.166667, 0.272166)--cycle;
	\fill[draw=blue, fill=blue, fill opacity=0.2] (0, 0,0.204124)--(-0.288675,0,0)--(0, 0, 0)--cycle;
	\fill[draw=blue, fill=blue, fill opacity=0.2] (0, 0,0.204124)--(-0.288675,0,0)--(-0.19245, 0, 0.272166)--cycle;
	\fill[draw=blue, fill=blue, fill opacity=0.2] (0, 0,0.204124)--(-0.144338, 0.25, 0.408248)--(-0.19245, 0, 0.272166)--cycle;
	\fill[draw=blue, fill=blue, fill opacity=0.2] (0, 0,0.204124)--(0.288675, 0, 0.408248)--(0.096225, 0.166667, 0.272166)--cycle;
	\fill[draw=blue, fill=blue, fill opacity=0.2] (0, 0,0.204124)--(0.144338, -0.25,0)--(0,0,0)--cycle;
	\fill[draw=blue, fill=blue, fill opacity=0.2] (0, 0,0.204124)--(0.288675, 0, 0.408248)--(0.096225, -0.166667, 0.272166)--cycle;
	\fill[draw=blue, fill=blue, fill opacity=0.2] (0, 0,0.204124)--(0.144338, -0.25,0)--(0.096225, -0.166667, 0.272166)--cycle;
	\fill[draw=blue, fill=blue, fill opacity=0.2] (0, 0,0.204124)--(-0.144338, -0.25, 0.408248)--(0.096225, -0.166667, 0.272166)--cycle;
	\fill[draw=blue, fill=blue, fill opacity=0.2] (0, 0,0.204124)--(-0.144338, -0.25, 0.408248)--(-0.19245, 0, 0.272166)--cycle;
	\end{tikzpicture} \hspace{2,5cm}
	\caption{This figure presents the $S^1$ fibration of $S^7$ which can be visualized by performing a cut on the polytope that gives four pieces of $S^1\times \mathbb C^3$.}\label{figure2}
\end{figure}
The following factorization of the result is possible
\begin{align} \label{factorS7}
S_4 (x|\vec{R}) \approx \prod_{k =1}^4 (z_k|q_k)_\infty \, ,
\end{align}
where $z_k = e^{2\pi i \frac{x}{\omega_k}}$ and $q_k = \left( e^{2\pi i \frac{\omega_1}{\omega_k}},\dots ,e^{2\pi i \frac{\omega_{k-1}}{\omega_k}},e^{2\pi i \frac{\omega_{k+1}}{\omega_k}},\dots,e^{2\pi i \frac{\omega_4}{\omega_k}}\right)$.
The idividual functions are called q-shifted factorials \cite{Narukawa}. The proportionality factor is associated to Bernoulli polynomials and out of the scope of this discussion. For further details refer to Paper \ref{lbl:paper1} and \cite{Narukawa}. 

It is worth mentioning that the q-shifted factorial corresponds to a perturbative Nekrasov partition function on $S^1 \times_\epsilon \mathbb C^3$, \cite{Nekrasov2008}. This hints to the Hopf decomposition of $S^7$ into four pieces around the closed Reeb orbits, similarly to 5d \cite{Qiu:2013aga,Qiu:2014oqa}. Locally in the neighbourhoods of the Reeb orbits the geometry is $S^1 \times_\epsilon \mathbb C^3$ where we have imposed some twisted periodic boundary condition on the $S^1$ corresponding to the $\mathbb{C}^3$.

That way one can write argue that the full partition function on $S^7$ can be factorized into four Nekrasov partition functions corresponding to the four closed Reeb orbits 
\begin{equation}
Z^{\text{full}}_{S^7} = \prod_{i=1}^4 Z^{\text{full}}_{S^1 \times \mathbb C ^3} (\beta_i, \epsilon_{1i}, \epsilon_{2i}\, , \epsilon_{3i})~,\label{full-PF-S7}
\end{equation}
This can be also seen by the polytope in Figure \ref{figure2} where each vertex corresponds only to one Reeb orbit non-degenerate, as it was also discusses in Section \ref{sec:toricsasakieinsteinintro}. The local geometry there is $S^1 \times \mathbb C ^3$. 

Also not that every Sasaki-Einstein manifold admits a similar factorization which can be found in Paper \ref{lbl:paper1}.

In the case of $S^7$ one can have a second Hopf fibration which is associated to the fact that, in contrast to $S^5$, the 7-sphere is also a 3-Sasaki manifold. Then this $SU(2)$-Hopf fibration does not pick only one complex structure but it reserves the properties of the $SU(2)\approx S^3$ complex structure.\footnote{For a small introduction on 3-Sasaki manifolds one can be referred to  Section \ref{sec:killingspinorsgeneral} and the references within.}  



\begin{figure}[h!]
		\centering
	\begin{tikzcd}
	S^7  \arrow[d] & S^3 \arrow[l]\\
	 S^4  & 
	\end{tikzcd}
\quad\quad \quad\quad
	\tdplotsetmaincoords{70}{115}
	\begin{tikzpicture}[tdplot_main_coords, scale=3,baseline={([yshift=-0.5ex]current bounding box.center)}]
	\draw[thick] (0.57735,0,0)--(-0.288675,0.5,0)--(-0.288675,-0.5,0)--cycle;
	\draw[thick] (0.57735,0,0)--(-0.288675,0.5,0)--(0,0,0.816497)--cycle;
	\draw[thick] (0.57735,0,0)--(-0.288675,-0.5,0)--(0,0,0.816497)--cycle;
	\draw[thick] (-0.288675,0.5,0)--(-0.288675,-0.5,0)--(0,0,0.816497)--cycle;
	
	\filldraw[thick , draw=black, fill=blue, fill opacity=0.2] (0.144338, -0.25,0)--(-0.288675,0,0)--(-0.144338, 0.25, 0.408248)--(0.288675,0,0.408248)--cycle;
	\end{tikzpicture}
	\caption{This figure presents the $S^1$ fibration of $S^7$ which can be visualized by performing a cut on the polytope that gives locally two copies of $S^3\times \mathbb C^2$.}\label{figure3}
\end{figure}
 

 Following the same thought process as before we have found that there is a second factorization of the quadruple sine, Paper \ref{lbl:paper1},
 \begin{equation}\label{eq:s4tos2s}
 S_4 (x|\vec \omega) =\prod_{j_{1,2}=0}^{\infty}S_2(x+ j_1 \omega_3 + j_2 \omega_4|\omega_1 , \omega_2)S_2(x- (j_1+1) \omega_1 - (j_2+1) \omega_2|\omega_3 , \omega_4)\, .
 \end{equation}
 
 As it is argued in Paper \ref{lbl:paper1} the double sine that is found in the above factorization is the perturbative part of the partition function on $S^3 \times \mathbb C^2$ so the perturbative part of the full partition function is 
 \begin{equation}
 Z^{\text{pert}}_{S^7}= \prod_{i=1}^2 Z^{\text{pert}}_{S^3\times \mathbb C^2} (a_{i1}, a_{i2}, \epsilon_{i1} , \epsilon_{i2})\, ,
 \end{equation}
 where there is a relation between the parameters $a_1, a_2$, which are associated with the $S^3$ and $\epsilon_1, \epsilon_2$ which are associated with the rotations in $\mathbb C^2$ as it is clear from \eqref{eq:s4tos2s}. 
 
 This factorization can be also visualized in the polytope, see Figure \ref{figure3} where each of the two parts after the cut has $S^3 \times \mathbb C^2$ local geometry.
 
 