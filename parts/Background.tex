\part{Review of geometry}\label{part:geometry}

This part serves as an introduction to the mathematical framework used throughout the thesis. 
The main technique, localization, reviewed in Part \ref{part:localization}, heavily uses notions from equivariant cohomology. Depending which dimension is considered, symplectic or contact geometry are needed to perform this technique. 

We will start by recalling basic concepts in geometry for even (symplectic) and odd dimensions (contact) with a compact Lie group action. Special attention will be given to Sasaki Einstein manifolds needed in Part \ref{part:7d}. Finally we introduce the notion of equivariant cohomology in general dimensions.

We assume the reader is familiar with basic geometric notions such as complex manifolds, differential forms and de Rham cohomology. \cite{Nakahara} is a thorough introduction on these notions. Also for symplectic or contact geometry see e.g. \cite{dasilva} and \cite{Schmude:2014lfa,Sparks:2010sn} respectively. 





\chapter{Symplectic and toric geometry}\label{sec:symplecticgeometry}

Let $\mathcal{M}$ be an even dimensional ($2n$) smooth manifold. This manifold is said to be symplectic if there exists a closed non-degenerate 2-form on $\mathcal{M}$, $\omega\in \Omega^2(\mathcal{M})$. Since $\omega$ is non-degenerate, $\omega^n\neq 0$, $\omega^n$ is a top form and it calculates the volume  of $\mathcal{M}$,thus it consists a volume form. Then $\omega$ is called symplectic form. Let us review some basic concepts related to symplectic geometry. For a thorough review see e.g. \cite{dasilva}.

A vector field $X_H$ is called Hamiltonian if there exist a function $H$ such that
\begin{equation}\label{eq:hamiltonianfunction}
\iota_{X_H} \omega= d H\, .
\end{equation}
Then $H$ is called also Hamiltonian function.

An important theorem is the Darboux theorem which says that any two symplectic manifolds of the same dimension are locally symplectomorphic to each other. This is important for our discussion because it means that every symplectic manifold locally looks the same as flat space of the same dimension so one can use in local patches of $\mathcal{M}$ the Darboux coordinates that bring the symplectic form into its standard form
\begin{equation}
\omega = \sum_{i=1}^n dq^i \wedge dp^i\, .
\end{equation}
As a result the symplectic form does not encode any global information. 

Furthermore a K\"ahler manifold is a symplectic manifold with a compatible almost complex structure $J$ such that
\begin{equation}
g(X,Y)= \omega (X,JY)\, .
\end{equation}
Note that $J^2=-1$. If there exist such structure the symplectic form is also called K\"ahler form.
One can compexify then the tangent bundle of the manifold using the decomposition $T
\mathcal M =T\mathcal M^+ \oplus T \mathcal{ M}^-$. Then one can decompose a differential form $\alpha$ accordingly. To clarify, let us look for example a space with complex coordinates $z_i), \bar z_i $. Then the base for $T\mathcal M^+$ is $dz_i$ and for $T\mathcal M^-$ is $d\bar z_i$. The differential form $\alpha$ can be expressed as
\begin{equation}
\alpha = \frac{1}{p! q!} \alpha_{\mu_1\ldots \mu_p \nu_{1}\ldots \nu_q} dz^{\mu_1}\wedge\ldots\wedge dz^{\mu_p}\wedge d\bar z^{\mu_1}\wedge\ldots\wedge dz^{\mu_q}
\end{equation}
then that form is called $(p,q)$-form, $\alpha \in \Omega^{(p,q)}(\mathcal M)$. More generally the space of $k$-forms can be decomposed to
\begin{equation}
\Omega^{k}(\mathcal M)=\sum_{p+q=k}\Omega^{(p,q)}(\mathcal M)
\end{equation}
due to the complex structure on $\mathcal M$.
One then can separate the differential into two Dolbeault operators $d=\partial +\bar \partial$ where they are the following maps
\begin{equation}\label{eq:Dolbeault}
\partial: \Omega^{(p,q)}(\mathcal M)\to \Omega^{(p+1,q)}(\mathcal M)\quad \text{and} \quad \bar \partial: \Omega^{(p,q)}(\mathcal M)\to \Omega^{(p,q+1)}(\mathcal M)\, .
\end{equation}
Using the Dolbeault operators one can define holomorphic $k$-forms to be any form $\alpha \in \Omega^{(k,0)}(\mathcal M)$ and it satisfies $\bar \partial \alpha =0$.



Now let us use these notions for a Group action.
Let G be a Lie group with a left group action on $\mathcal{M}$, $\sigma:G\times \mathcal{M}\to \mathcal{M}$. We are going to restrict ourselves to compact Lie groups since we want in the end to use toric Lie groups $T^n$. 
Also let $\mathfrak{g}$ be the algebra of $G$ and $X$ one of its elements $X\in \mathfrak{g}$. Then the vector field $X^\sharp$ generated by $X$ ($\{e^{tX}|t\in \mathbb{R}\}$) is called fundamental vector field and it generates the flow in the direction of $X$. We also denote with $\mathfrak{g}^\ast$ the dual Lie algebra and with $\langle \cdot , \cdot \rangle: \mathfrak{g}\times\mathfrak{g}^\ast  \to \mathbb{R}$ the pairing between them.  

We call the left Lie group action Hamiltonian if there exist a map $\mu : \mathcal{M}\to \mathfrak{g}^\ast$ such that
\begin{equation}\label{eq:momentmap}
d\langle \mu, X\rangle = \iota_{X^\sharp} \omega
\end{equation}
and it is equivariant with respect to the action $\sigma$. Then the map $\mu$ is called moment map and the group action $G$ Hamiltonian. For Abelian groups, which are the ones we are interested in, equivariant means $\mu \circ \sigma = \mu$. Furthermore by comparison with \eqref{eq:hamiltonianfunction}, the Hamiltonian function takes the form $H= \langle \mu, X\rangle$.

The moment map $\mu$ gives hints in analysing the geometry where a very useful tool is the moment map cone. According to \cite{Atiyah82}, a connected symplectic manifold $(\mathcal{M},\omega)$ with torus Hamiltonian action $T^n$ has a moment map $\mu$ whose image is a convex hull which represent the images of the fixed points of the action. This polytope is called moment map cone.

Moreover we will say a few things about symplectic quotients. One can use the moment map in order to reduce the original manifold $\mathcal{M}$ to a lower dimensional one. More specifically if $G$ acts freely on $\mu^{-1}(0)$ then the manifold 
\begin{equation}\label{eq:symplecticquotient}
\mathcal{M}//G := \mu^{-1}(0)/G
\end{equation}
is a symplectic manifold which is also referred to as the symplectic quotient of $\mathcal{M}$. If the original manifold is also K\"ahler then the quotient is called K\"ahler quotient and resulting manifold also inherit the complex structure so it is also K\"ahler. It is worth mentioning that even if the original space is flat the resulting manifold, after the quotient, can be highly non-trivial. An example can be the Sasaki-Einstein manifolds that we are discussing in Section \ref{sec:toricsasakieinsteinintro} and they can be viewed as K\"ahler quotients, e.g. a 7-dimensional example is \cite{Martelli:2008rt}.

Finally a Calabi-Yau manifold is K\"ahler manifolds that has vanishing Ricci tension, $R_{\mu\nu}=0$. An example of such a manifold is the flat space $\mathbb{C}^n$. For a more detailed review of Toric Geometry we refer the reader to \cite{fulton}.

\subsection*{Example}\label{subsec:examplesymplectic}
Let us consider an example in order to absorb these notions. We assume $\mathbb{C}^n$ with a $G=U(1)$ action acting from the left, $U(1)\times \mathbb{C}^n\to \mathbb{C}^n$.

The standard symplectic form is
\begin{equation}
\omega={1\over 2} \sum_n dz_i \wedge d\bar z_i\, .
\end{equation}
The group action $U(1)=S^1$ can be parametrized by $e^{i\theta}$ where $\theta$ has the notion of an angle. Then the group acts, $\sigma:S^1\times \mathbb{C}^n\to \mathbb{C}^n$, as rotation on the coordinates
\begin{align}
(e^{i\theta},(z_i,\bar z_i))\mapsto (e^{i\theta}z_i, e^{-i\theta}\bar z_i).
\end{align}
The angle $\theta$ gives the element of the Lie algebra $\mathfrak{g}\simeq \mathbb R$. If we assume the standard basis vectors on $\mathbb C^n$ then the fundamental vector field takes the form
\begin{equation}\label{eq:killingflat}
X^\sharp = \sum_{i=1}^n (z_i \partial_{z_i}- \bar z_i \partial_{\bar z_i})\, .
\end{equation}
The moment map can be found using \eqref{eq:momentmap}. Since the algebra is $\mathbb{R}$ we can use the standard basis on it, $e$, so $\langle \mu , X\rangle= \langle \mu , e\rangle= \mu_e$ where $\mu:\mathbb{C}\to R^\ast\simeq R$. Then
\begin{align}
d\mu_e = \iota_{X^\sharp} \omega=\frac{1}{2} \sum_{i} (z_i d\bar{z}_i + \bar z_i dz_i)= d(\frac{1}{2}\sum_{i} |z_i|^2)\, .
\end{align}
As a result there is a family of solutions for the moment map
\begin{equation}
\mu(z)= \frac{1}{2}\sum_{i} |z_i|^2 + c\, ,
\end{equation}
where $c$ is an arbitrary constant. Note that the moment map is indeed equivariant since $\mu(e^{i\theta}) = \mu(z)$.


The image of $\mu$ is just $\mathbb{R}_{\geq 0 }$ which is the moment map cone. Now let us assume that $c=1/2$ then in order to find the symplectic quotient we have to find $\mu =0$ according to \eqref{eq:symplecticquotient}
\begin{equation}
\sum_{i}|z_i|^2 =1 \implies \mu^{-1}(0)=S^{2n-1}\, ,
\end{equation}
which is the unit sphere. Also the $S^1$ acts freely on the unit sphere so the quotient $S^{2n-1}/S^1$ is also a symplectic manifold and more specific it is the $\mathbb{C}P^{n-1}$. What we just described is a principal $U(1)$-bundle
\begin{align}
S&^{2n-1}\leftarrow S^1\nn\\
&\downarrow\nn\\
\mathbb{C}&P^{n-1}
\end{align}
which is also called a Hopf fibration.


We will also consider the generalization of this example where we pick $T^n$ as the action group. The Lie algebra and its dual are both $\mathbb{R}^n$. One can pick the standard basis on $\mathbb{R}^n$, $e_a$, so that the Hamiltonian function is $\mu_a = \langle \mu,e_a\rangle$ and the moment map $\mu: \mathbb{C}^n\to \mathbb{R}^n$ would have a similar form depending on the specific action of the group.

The toric action we consider is $T^n\times \mathbb{C}^n \to \mathbb{C}^n$, where 
\begin{equation}\label{eq:torusactiontn}
z_i \to e^{i\theta _i} z_i\, .
\end{equation}
In this case, the moment map cone is of the form 
\begin{align}
\mu : \mathbb{C}^n \to \mathbb{R}^n\nn\\
\mu=\frac{1}{2}(|z_1|^2,\ldots |z_n|^2)\, . \label{eq:cnmomentmap}
\end{align}

It is easy to see that the image of the moment map is the cone $\mathbb{R}^n_{\geq 0}$. The moment map cone is non compact since $\mathbb{C}^n$ is not compact. One can see that there is only one fixed point which is the corner of the polytope ($\mu=0$) which according to \eqref{eq:cnmomentmap} is the centre of $\mathbb{C}^n$ ($z_i=0$). 
In the case of a compact manifold we would end up with the polygon as a moment map cone. Such an example will be shown in the case of odd-dimensional spheres in Figure \ref{figurespheres}.

\chapter{Contact geometry}\label{sec:constactgeometry}

We will review the most important concepts of contact geometry, which is the analogue of symplectic geometry for odd dimensions. Specifically we will focus on Sasaki-Einstein manifolds with toric action, since, as we will discuss further on, they allow for supersymmetry. For a more apprehended review of the matter can be found in the textbooks \cite{Blair2010} and \cite{Geiges} or the Lecture notes \cite{etnyre}.

A contact manifold is an ($2n+1$)-dimensional manifold $\mathcal{M}$ equipped with a contact form $\kappa$. The contact form $\kappa$ is a 1-form that satisfies $\kappa\wedge (d\kappa)^n \neq 0$, which gives the volume form on $\mathcal{M}$. There exist a unique vector $R$ associated with $\kappa$, such that
\begin{equation}\label{eq:defcontactreeb}
\iota_R d\kappa=0 \quad \text{and}\quad \iota _R \kappa=1.
\end{equation}
This vector field is called the Reeb vector.  

The tangent bundle of $\mathcal{M}$ can be decomposed into the  line tangent to $R$ or "Reeb direction" and the hyperplane that is defined by the kernel of $\kappa$ or "horizontal space".
It can be noted that $d\kappa$ when restricted on the horizontal space defines a symplectic form on it. This is a connection between contact manifolds and symplectic manifolds in one dimension lower.

If $\mathcal{M}$ is also Riemannian, i.e. it is equipped with a metric $g$, then we can define a K-contact manifold by imposing that:
\begin{itemize}
	\item The Reeb vector is also a Killing vector, i.e. $\mathcal{L}_R g=0$.
	\item There exists a (1,1)-tensor $J$ satisfying
	\begin{align}
	g(X,Y)= d\kappa (X,JX)\, ,\\
	g(JX,JY)= g(X,Y)-\kappa(X)\kappa (Y)\, ,\\
	J^2 = -1 + \kappa \otimes R\, .
	\end{align}
\end{itemize} 

In that case the volume form of the manifold is given by
\begin{equation}
\text{Vol}= \frac{(-1)^n}{2^n n!}\kappa \wedge d\kappa.
\end{equation}

Similarly to the decomposition of the tangent bundle we mentioned above, there is a decomposition of differential forms that is going to be very useful in Part \ref{part:7d}. Using the contact form one can define the projectors
\begin{align}\label{eq:contactprojectors}
P_V = \kappa\wedge \iota_R \quad \text{and}\quad P_H= 1-P_V
\end{align}
with the projection relation $P_{V/H}^2 =P_{V/H}$.

These projectors decompose forms into a "vertical" and a "horizontal" part
\begin{equation}\label{eq:decompositionforms}
\Omega^r = \Omega^r_V \oplus \Omega^r _H.
\end{equation}

It is important to note that since the horizontal space inherits the complex structure it can be decomposed into $(p,q)$-forms where Dolbeault operators act as \eqref{eq:Dolbeault} and as a result \eqref{eq:decompositionforms} can be further decomposed in
\begin{equation}\label{eq:contactfulldecompostition}
\Omega^r = \bigoplus_{p+q = r} \Omega_H^{(p,q)}\oplus \Omega^{(k-1)}_H \kappa.
\end{equation}
One may also want to decompose it one step further and extract $d\kappa$ from the horizontal forms which is what was done in Table \ref{table:cohomologicalcomplex}.



\section{Toric Sasaki-Einstein manifolds}\label{sec:toricsasakieinsteinintro}
An interesting subset of contact manifolds that we are going to use is the Sasaki-Einstein manifolds that admit a torus action.  A comprehensive introduction can be found in \cite{dasilva} and \cite{Sparks:2010sn}. 

We will start with the definition of a Sasaki manifold. A Sasaki manifold $\mathcal{M}$ is a manifold that its metric cone $C(\mathcal M)$ is a K\"ahler manifold. This means that there exist a K\"ahler manifold $C(\mathcal M)$ which is of the form $C(\mathcal M)=\mathcal M \times \mathbb{R}_{\geq 0}$ with metric
\begin{equation}
ds^2_C = dr^2 + r^2 ds^2_{\mathcal M}\, ,
\end{equation}
where $r$ is the coordinate of $\mathbb{R}_{\geq 0}$ and $\mathcal M$ is the base of the cone. Then one can relate the geometric data of the two manifolds as follows. The symplectic form on $C(\mathcal M)$ is $\omega_C = d(r^2 \kappa)$, where $\kappa$ is the contact form on $\mathcal M$ with associated Reeb vector $R$. The almost complex structure on $C(\mathcal M)$ is also defined using the base contact manifold as $J_C(X) = J(X) - \kappa(X)r \partial_r$, where $J_C(r \partial_r )= R$.

A Sasaki-Einstein manifold is a manifold whose cone is a Calabi-Yau. This equivalently means that a Sasaki-Einstein manifold is simply a Sasaki manifold that admits an Einstein metric\footnote{It satisfies the Einstein vacuum equation} i.e. the Ricci tensor is proportional to the metric
\begin{equation}
R_{\mu\nu}= \lambda g_{\mu\nu}\, ,
\end{equation}
for a cosmological constant $\lambda$. For a Sasaki-Einstein manifold of 2n-1 dimension, the cosmological constant is $\lambda = 2 n -2$.

As it was mentioned, the relevant manifolds for this thesis are the toric Sasaki-Einstein manifolds. This means that the cone $C(\mathcal M)$ admits a Hamiltonian action that respects its complex structure. Also the Reeb vector needs to lie on the algebra of the $T^n$ action. In that case, there exist a moment map and a moment map cone for them, which is going to be non-compact. The basis of this cone is a compact (n-1)-dimensional polytope called Delzant polytope and it is the one that is associated to the Sasaki-Einstein manifold. It needs to be noted that since the Reeb vector is part of the toric algebra it can be represented by a vector on the convex cone. As in the case of the moment map cone the Delzant polytope gives important information about the manifold and its action. In particular the Sasaki-Einstein manifold is a $T^n$ fibration over the Delzant polytope. At each face one $S^1$ of the action degenerates. Since the polytope is $n-1$ dimensional and the action is n-dimensional, that a combination of the circles never degenerates at the vertices which gives rise to a Hopf fibration.
{\color{red} Maybe add the inward pointing normals and the 1-Gorenstein condition?}
\subsection*{Example}
An example is  of a Sasaki-Einstein manifold is the sphere $S^{2n-1}$. Its cone is $\mathbb{C}^n$ so we will consider a $T^n$ action on it as in the example in Section \ref{subsec:examplesymplectic}. The torus acts as in \eqref{eq:torusactiontn} and as a result the moment map of the cone $C(\mathcal M)$ is of the form \eqref{eq:cnmomentmap}.
We recall that the image of the moment map is $\mathbb{R}^n_{\geq 0}$, where the axis are $|z_i|^2$, see Figure \ref{figurespheres}. Since the Reeb vector lies on the Lie algebra of the Torus action, we can represent it with the vector $R=(1,\ldots, 1)$.

\begin{figure}[h!]
	\centering
	\tdplotsetmaincoords{0}{0}
	\begin{tikzpicture}[tdplot_main_coords, scale=1.4,baseline={([yshift=-0.5ex]current bounding box.center)}]
	\draw[thick,->] (0,0,0)--(2,0,0);
	\draw[thick,->] (0,0,0)--(0,2,0);
	\draw[thick,color=blue,opacity=0.2] (1,0,0)--(0,1,0)--cycle;
	\node at (1.5,1.5,0) {$S^3$};
	\end{tikzpicture}	
	\tdplotsetmaincoords{70}{115}
	\begin{tikzpicture}[tdplot_main_coords, scale=1.4,baseline={([yshift=-0.5ex]current bounding box.center)}]
	\draw[thick,->] (0,0,0)--(2,0,0);
	\draw[thick,->] (0,0,0)--(0,2,0);
	\draw[thick,->] (0,0,0)--(0,0,2);
	\draw[thick] (1,0,0)--(0,1,0)--(0,0,1)--cycle;
	\filldraw[thick , draw=black, fill=blue, fill opacity=0.2] (1,0,0)--(0,1,0)--(0,0,1)--cycle;
	\node at (0,1.5,1.5) {$S^5$};
	\end{tikzpicture}
	\tdplotsetmaincoords{70}{115}
	\begin{tikzpicture}[tdplot_main_coords, scale=3,baseline={([yshift=-0.5ex]current bounding box.center)}]
	\draw[thick] (0.57735,0,0)--(-0.288675,0.5,0)--(-0.288675,-0.5,0)--cycle;
	\draw[thick] (0.57735,0,0)--(-0.288675,0.5,0)--(0,0,0.816497)--cycle;
	\draw[thick] (-0.288675,0.5,0)--(-0.288675,-0.5,0)--(0,0,0.816497)--cycle;
	\filldraw[thick , draw=black, fill=blue, fill opacity=0.2] (0.57735,0,0)--(-0.288675,0.5,0)--(0,0,0.816497)--cycle;;
	\filldraw[thick , draw=black, fill=blue, fill opacity=0.2](0.57735,0,0)--(-0.288675,-0.5,0)--(0,0,0.816497)--cycle;
		\node at (0,0.7,0.7) {$S^7$};
	\end{tikzpicture}

	\caption{Examples of moment map cones together with the polytopes of the odd-spheres (blue filling).}\label{figurespheres}
\end{figure}

The image of the moment map of the Sasaki-Einstein manifold $S^{2n-1}$ can be viewed as a hypersurface of the cone $C(\mathcal{M})$ when $r=1$.

For example in the case of $S^3$, the cone is $\mathbb{C}^2$ with $T^2$ action and the polytope is an interval as it is shown in the left of Figure \ref{figurespheres}. Therefore one can think of the $S^3$ as a $T^2$ fibration over the interval where as we go closer to the boundaries one of the circles degenerates. At the neighbourhood of the boundaries the local geometry is $S^1 \times \mathbb{C}$. Also one can take a linear combination of the two circles which does not degenerated and any point on the interval. This gives rise to the usual Hopf fibration $S^1\rightarrow S^3 \rightarrow\mathbb{C} P^1$.   

As one goes to more interesting examples the polytope distorts in shape but there have being tools developed to read of the geometric data. We can refer the interested reader to \cite{Winding:2016wpw}. 


\chapter{Equivariant cohomology}\label{sec:equivariantcohomology}

Let's consider a smooth n-dimensional manifold $\mathcal M$ with $G$ action. The idea of equivariant cohomology is to take into consideration the group action into the formed cohomology. 

In the case of a free action $G$ then the quotient space $\mathcal{M}/G$ is a smooth manifold and we can define de Rham cohomology there, $H^\cdot (\mathcal M /G)$. If the action is not free and there exist fixed points then the quotient is not a smooth manifold and the situation is more complicated. 

%In this case we can define $G$-equivariant cohomology going to the space $(\mathcal M \times EG)/G$ where $EG$ is the universal bundle of $G$, \cite{Milnor}. 

In order to understand this better we will continue using the Cartan model, \cite{Cartan}\footnote{It should be also noted that there exist also other models such as the Weil model, but they are out of the scope of this thesis}. We are going to follow the review \cite{Pestun:2016qko}. 

Let as assume a Lie algebra $\mathfrak{g}$ with elements $\phi=\phi^a T_a$, where $\phi^a$ can be thought of an element of the dual algebra $\mathfrak{g}^\ast$. For the Cartan model we will need the symmetric algebra of $\mathfrak{g}^\ast$ which we will denote as $S(\mathfrak{g}^\ast)\approx \mathbb{R}[\mathfrak{g}]$, where we denote with $\mathbb{R}[\mathfrak{g}]$ the  commutative ring of polynomial functions on the vector space underlying $\mathfrak{g}$ and its elements are polynomials of $\{\phi^a\}$.

We will introduce the form $\alpha\in \Omega^\bullet(\mathcal M)\otimes \mathbb{R}[\mathfrak{g}]$ which are invariant under the $G$-action. This form is not homogeneous with respect to the degree of the form but rather it is a sum of forms of various degrees
\begin{equation}\label{eq:polyform}
\alpha = \sum_{k=0}^{n} \alpha_k
\end{equation}
where $\alpha_k$ is a k-form.
For every element $T_a$, the group action on $\mathcal{M}$ induces a vector field $v_a$ on $\mathcal{M}$, which points in the direction of the flow of  that group element. The vector field $v=\phi^a \varepsilon_a$, is the fundamental vector field that points to the direction of the flow of the full group. 

The following Cartan differential can be defined which acts on $\alpha$
\begin{align}\label{eq:eqdifdef}
d_v : \Omega^\bullet(\mathcal M)&\otimes \mathbb{R}[\mathfrak{g}]\to \Omega^\bullet(\mathcal M)\otimes \mathbb{R}[\mathfrak{g}]\nn\\
&d_v= d+ \iota_v
\end{align} 
where $d: \Omega^\bullet (\mathcal M)\to \Omega^{\bullet+1}(\mathcal M)$ is the de Rham differential and $\iota_v:\Omega^\bullet (\mathcal M)\to \Omega^{\bullet-1}(\mathcal M)$ is the contraction fo the differential form and the fundamental vector field. We will refer to this differential as the equivariant differential along $v$. An important property is that the equivariant differential squares to the Lie derivative along the vector field $v$
\begin{eqnarray}
d_v^2=\mathcal L_v
\end{eqnarray} 

As it is argued in \cite{Pestun:2016qko}, if the form $\alpha$ is $G$-invariant then $d_v ^2\alpha = \mathcal{L}_v \alpha =0$, which defines the Cartan model of equivariant cohomology $H^\bullet_G (\mathcal{M})= H((\Omega^\bullet(\mathcal M)\otimes \mathbb{R}[\mathfrak{g}])^G, d_v)$.
The invariant form $\alpha$ then is called equivariantly closed form, i.e. $d^2_v \alpha=0$. In the case of $\alpha = d_v \lambda$, where $\lambda \in \Omega^\bullet(\mathcal M)\otimes \mathbb{R}[\mathfrak{g}]$ then $\alpha$ is called equivariantly exact. 

As it was mentioned in \eqref{eq:polyform}, $\alpha$ is actually a polyform. This means that the constrain that it is equivariant closed is actually a relation between the different degrees of $\alpha$
\begin{equation}
d_v^2 \alpha =0 \implies d \alpha_{k-2} + \iota_v \alpha_k = 0 \quad\forall\, k\in[0,n]
\end{equation}

A final remark is that when we integrate a polyform $\alpha$ over the full manifold only the top-form gives a contribution
\begin{equation}
	\int_{\mathcal M}\alpha = \int_{\mathcal M}\alpha_n\,.
\end{equation}

\subsection*{Supersymmetric example}

In order to make contact with Part \ref{part:localization} we will use the Cartan model in the context of supergeometry.

Let $\{x^\mu\}$ being the usual coordinates and we will introduce also the Grassmann coordinates $\psi^\mu$. These coordinates are anti-commuting , i.e. $\psi^\mu \psi^\nu = -\psi^nu \psi^\mu$. These coordinates are fermionic degrees of freedom. The even coordinates $x^\mu$ are going to be the bosonic degrees of freedom.

As it is known the fermionic coordinates have the following integration rules
\begin{equation}
I=\int (\prod_{\mu=1}^{k} \psi^\mu) \, d^n\psi = 1 \quad \text{if}\quad k=n \quad \text{otherwise}\quad I=0 \, ,
\end{equation}   
which mimics the way the equivariant form is manipulated. 

In the specific case where the super-manifold is the odd tangent bundle $\Pi T \mathcal M$, the odd coordinates correspond to the 1-forms $dx^\mu$ and its multiplication corresponds to the Wedge product between forms. 
Then the equivariant differential is represented as a transformation on our coordinates 
\begin{align}\label{eq:easytranformation}
d_v x^\mu = d x^\mu = \psi^\mu\nn\\
d_v \psi ^\mu =v^\mu\, .
\end{align}
In the physics language these transformations are denoted with $\delta$ as we are going to follow in the next Parts.
